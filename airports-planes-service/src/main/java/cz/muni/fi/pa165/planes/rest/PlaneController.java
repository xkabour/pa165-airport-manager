package cz.muni.fi.pa165.planes.rest;

import cz.muni.fi.pa165.api.plane.Plane;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.api.plane.requests.PlaneRequest;
import cz.muni.fi.pa165.planes.facade.PlaneFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class PlaneController {

    private final PlaneFacade planeFacade;

    @PostMapping("/plane")
    public Plane create(@RequestBody PlaneRequest request) {
        return planeFacade.create(request);
    }


    @GetMapping("/plane/{id}")
    public Plane get(@PathVariable UUID id) {
        return planeFacade.get(id);
    }

    @PutMapping("/plane/{id}")
    public Plane updateName(@PathVariable UUID id, @RequestParam String name) {
        return planeFacade.updateName(id, name);
    }

    @GetMapping("/plane/type")
    public List<Plane> getByType(@RequestParam PlaneType planeType) {
        return planeFacade.getByType(planeType);
    }
}
