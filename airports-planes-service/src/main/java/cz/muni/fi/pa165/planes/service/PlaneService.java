package cz.muni.fi.pa165.planes.service;

import cz.muni.fi.pa165.planes.dao.Plane;
import cz.muni.fi.pa165.planes.dao.PlaneType;
import cz.muni.fi.pa165.planes.exception.PlaneNotFoundException;
import cz.muni.fi.pa165.planes.repository.PlaneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PlaneService {

    private final PlaneRepository planeRepository;

    public Plane create(Plane plane) {
        return planeRepository.save(plane);
    }

    public Plane get(UUID id) {
        return planeRepository.findById(id).orElseThrow(PlaneNotFoundException::new);
    }

    public Plane updateName(UUID id, String name) {
        var plane = planeRepository.findById(id).orElseThrow(PlaneNotFoundException::new);
        if (name != null) {
            plane.setName(name);
        }
        return planeRepository.save(plane);
    }

    public List<Plane> getByType(PlaneType planeType) {
        return planeRepository.findByPlaneType(planeType);
    }
}
