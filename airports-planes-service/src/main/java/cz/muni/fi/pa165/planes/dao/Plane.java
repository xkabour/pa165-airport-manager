package cz.muni.fi.pa165.planes.dao;

import cz.muni.fi.pa165.planes.Constants;
import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "plane", schema = Constants.PLANES_SCHEMA)
public class Plane {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String name;
    private PlaneType planeType;
    private int maxCapacity;
    private short pilotsRequired;

}
