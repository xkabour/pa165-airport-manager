package cz.muni.fi.pa165.planes.repository;

import cz.muni.fi.pa165.planes.dao.PlaneType;
import cz.muni.fi.pa165.planes.dao.Plane;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PlaneRepository extends CrudRepository<Plane, UUID> {

    List<Plane> findByPlaneType(PlaneType planeType);
}
