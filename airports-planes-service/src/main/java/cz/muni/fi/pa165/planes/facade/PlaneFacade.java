package cz.muni.fi.pa165.planes.facade;

import cz.muni.fi.pa165.api.plane.Plane;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.api.plane.requests.PlaneRequest;
import cz.muni.fi.pa165.planes.service.PlaneService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PlaneFacade {

    private final PlaneService planeService;
    private final ModelMapper modelMapper = new ModelMapper();

    public Plane create(PlaneRequest planeRequest) {
        var result = planeService.create(modelMapper.map(planeRequest, cz.muni.fi.pa165.planes.dao.Plane.class));
        return modelMapper.map(result, Plane.class);
    }

    public Plane get(UUID id) {
        var result = planeService.get(id);
        return modelMapper.map(result, Plane.class);
    }

    public Plane updateName(UUID id, String name) {
        var result = planeService.updateName(id, name);
        return modelMapper.map(result, Plane.class);
    }

    public List<Plane> getByType(PlaneType planeType) {
        var result = planeService.getByType(modelMapper.map(planeType, cz.muni.fi.pa165.planes.dao.PlaneType.class));
        return result.stream().map(p -> modelMapper.map(p, Plane.class)).toList();
    }
}
