package cz.muni.fi.pa165.planes.rest;

import cz.muni.fi.pa165.api.plane.Plane;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.planes.exception.PlaneNotFoundException;
import cz.muni.fi.pa165.planes.facade.PlaneFacade;
import cz.muni.fi.pa165.planes.utils.ObjectConverter;
import cz.muni.fi.pa165.planes.utils.TestApiFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {PlaneController.class})
@AutoConfigureMockMvc(addFilters = false)
class PlaneControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlaneFacade planeFacade;

    @Test
    void findById_planeFound_returnsPlane() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        Mockito.when(planeFacade.get(id)).thenReturn(TestApiFactory.getPlaneEntity());

        String responseJson = mockMvc.perform(get("/plane/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Plane response = ObjectConverter.convertJsonToObject(responseJson, Plane.class);

        assertThat(responseJson).isEqualTo("{\"id\":\"00000000-0000-0001-0000-00000000000f\",\"name\":\"Name\",\"planeType\":\"AIRBUS_A380\",\"maxCapacity\":600,\"pilotsRequired\":2}");
        assertThat(response).isEqualTo(TestApiFactory.getPlaneEntity());
    }

    @Test
    void findById_planeNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(planeFacade.get(id)).thenThrow(new PlaneNotFoundException());

        mockMvc.perform(get("/plane/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        Mockito.verify(planeFacade, Mockito.times(1)).get(id);
    }

    @Test
    void updatePlaneName_planeFound_returnsUpdatedPlane() throws Exception {
        UUID id = UUID.randomUUID();
        String updatedName = "UpdatedName";
        Plane updatedPlane = TestApiFactory.getPlaneEntity();
        updatedPlane.setName(updatedName);

        Mockito.when(planeFacade.updateName(Mockito.eq(id), Mockito.eq(updatedName))).thenReturn(updatedPlane);

        String responseJson = mockMvc.perform(put("/plane/{id}", id)
                        .param("name", updatedName))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        Plane response = ObjectConverter.convertJsonToObject(responseJson, Plane.class);

        assertThat(response).isEqualTo(updatedPlane);
        Mockito.verify(planeFacade, Mockito.times(1)).updateName(Mockito.eq(id), Mockito.eq(updatedName));
    }

    @Test
    void updatePlaneName_planeNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        String updatedName = "UpdatedName";

        Mockito.when(planeFacade.updateName(Mockito.eq(id), Mockito.eq(updatedName))).thenThrow(new PlaneNotFoundException());

        mockMvc.perform(put("/plane/{id}", id)
                        .param("name", updatedName))
                .andExpect(status().isNotFound());

        Mockito.verify(planeFacade, Mockito.times(1)).updateName(Mockito.eq(id), Mockito.eq(updatedName));
    }

    @Test
    void getByType_returnsPlanes() throws Exception {
        PlaneType planeType = PlaneType.AIRBUS_A380;
        List<Plane> planes = new ArrayList<>();
        Plane plane1 = TestApiFactory.getPlaneEntity();
        Plane plane2 = TestApiFactory.getPlaneEntity();
        plane2.setPlaneType(planeType);
        planes.add(plane1);
        planes.add(plane2);

        Mockito.when(planeFacade.getByType(planeType)).thenReturn(planes);

        mockMvc.perform(get("/plane/type").param("planeType", planeType.toString()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}