package cz.muni.fi.pa165.planes.utils;

import cz.muni.fi.pa165.planes.dao.Plane;
import cz.muni.fi.pa165.planes.dao.PlaneType;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TestDaoFactory {

    public static Plane getPlaneEntity() {
        Plane plane = new Plane();
        plane.setName("Name");
        plane.setPlaneType(PlaneType.AIRBUS_A380);
        plane.setMaxCapacity(600);
        plane.setPilotsRequired((short) 2);

        plane.setId(new UUID(0x1, 0xf));

        return plane;
    }

}
