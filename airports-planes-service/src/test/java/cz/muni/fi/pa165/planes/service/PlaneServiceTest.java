package cz.muni.fi.pa165.planes.service;

import cz.muni.fi.pa165.planes.dao.Plane;
import cz.muni.fi.pa165.planes.dao.PlaneType;
import cz.muni.fi.pa165.planes.exception.PlaneNotFoundException;
import cz.muni.fi.pa165.planes.repository.PlaneRepository;
import cz.muni.fi.pa165.planes.utils.TestDaoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class PlaneServiceTest {

    @Mock
    private PlaneRepository planeRepository;

    @InjectMocks
    private PlaneService planeService;

    @Test
    void createPlane() {
        Plane plane = TestDaoFactory.getPlaneEntity();
        Mockito.when(planeRepository.save(plane)).thenReturn(plane);
        assertEquals(plane, planeService.create(plane));
        Mockito.verify(planeRepository, Mockito.times(1)).save(plane);
    }

    @Test
    void getById_planeFound_returnsPlane() {
        UUID id = new UUID(0x1, 0xf);
        Plane plane = TestDaoFactory.getPlaneEntity();
        // Arrange
        Mockito.when(planeRepository.findById(id)).thenReturn(Optional.of(plane));

        // Act
        Plane foundEntity = planeService.get(id);

        // Assert
        assertThat(foundEntity).isEqualTo(plane);
    }

    @Test
    void getById_planeNotFound_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();

        assertThrows(PlaneNotFoundException.class, () -> planeService.get(id));
    }

    @Test
    void updatePlane_planeFound() {
        UUID id = new UUID(0x1, 0xf);
        Plane existingPlane = TestDaoFactory.getPlaneEntity();

        String update = "newName";

        Plane updatedPlane = TestDaoFactory.getPlaneEntity();
        updatedPlane.setName("newName");

        Mockito.when(planeRepository.findById(id)).thenReturn(Optional.of(existingPlane));
        Mockito.when(planeRepository.save(Mockito.any(Plane.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertEquals(updatedPlane, planeService.updateName(id, update));
        Mockito.verify(planeRepository, Mockito.times(1)).save(updatedPlane);
    }

    @Test
    void updatePlane_planeNotFound_throwsPlaneNotFoundException() {
        UUID id = UUID.randomUUID();
        String update = "newName";

        assertThrows(PlaneNotFoundException.class, () -> Optional
                .ofNullable(planeService.updateName(id, update))
                .orElseThrow(PlaneNotFoundException::new));

        Mockito.verify(planeRepository, Mockito.times(0)).save(Mockito.any());
    }

    @Test
    void getByType() {
        Plane plane1 = TestDaoFactory.getPlaneEntity();
        Plane plane2 = TestDaoFactory.getPlaneEntity();
        plane2.setId(UUID.randomUUID());
        List<Plane> planesSet = List.of(plane1, plane2);
        // Arrange
        Mockito.when(planeRepository.findByPlaneType(PlaneType.AIRBUS_A380)).thenReturn(planesSet);

        // Act
        List<Plane> foundEntity = planeService.getByType(PlaneType.AIRBUS_A380);

        // Assert
        assertThat(foundEntity).isEqualTo(planesSet);
    }
}