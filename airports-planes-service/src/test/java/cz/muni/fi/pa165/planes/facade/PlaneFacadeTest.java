package cz.muni.fi.pa165.planes.facade;

import cz.muni.fi.pa165.api.employee.requests.EmployeeRequest;
import cz.muni.fi.pa165.api.employee.requests.StewardRequest;
import cz.muni.fi.pa165.api.plane.Plane;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.api.plane.requests.PlaneRequest;
import cz.muni.fi.pa165.planes.exception.PlaneNotFoundException;
import cz.muni.fi.pa165.planes.service.PlaneService;
import cz.muni.fi.pa165.planes.utils.TestApiFactory;
import cz.muni.fi.pa165.planes.utils.TestDaoFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PlaneFacadeTest {

    @Mock
    private PlaneService planeService;

    @InjectMocks
    private PlaneFacade planeFacade;

    @Test
    void createPlane() {
        cz.muni.fi.pa165.api.plane.Plane apiPlane = TestApiFactory.getPlaneEntity();
        apiPlane.setId(null);
        cz.muni.fi.pa165.planes.dao.Plane daoPlane = TestDaoFactory.getPlaneEntity();
        daoPlane.setId(null);
        PlaneRequest request = TestApiFactory.getPlaneRequest();

        Mockito.when(planeService.create(daoPlane)).thenReturn(daoPlane);
        assertEquals(apiPlane, planeFacade.create(request));
        Mockito.verify(planeService, Mockito.times(1)).create(daoPlane);
    }

    @Test
    void getById_planeFound_returnsPlane() {
        var id = new UUID(0x1, 0xf);
        cz.muni.fi.pa165.api.plane.Plane apiPlane = TestApiFactory.getPlaneEntity();
        cz.muni.fi.pa165.planes.dao.Plane daoPlane = TestDaoFactory.getPlaneEntity();
        PlaneRequest request = TestApiFactory.getPlaneRequest();
        when(planeService.get(id)).thenReturn(daoPlane);

        var result = planeFacade.get(id);
        assertNotNull(result);

        verify(planeService, times(1)).get(id);
    }

    @Test
    void getById_planeNotFound_throwsPlaneNotFoundException() {
        UUID id = UUID.randomUUID();
        when(planeService.get(id)).thenThrow(new PlaneNotFoundException());
        assertThrows(PlaneNotFoundException.class, () -> planeFacade.get(id));
    }

    @Test
    void updatePlane_PlaneFound() {
        var id = new UUID(0x1, 0xf);
        cz.muni.fi.pa165.api.plane.Plane apiPlane = TestApiFactory.getPlaneEntity();
        cz.muni.fi.pa165.planes.dao.Plane daoPlane = TestDaoFactory.getPlaneEntity();

        Mockito.when(planeService.updateName(id, daoPlane.getName())).thenReturn(daoPlane);
        assertEquals(planeFacade.updateName(id, apiPlane.getName()), apiPlane);
        verify(planeService, times(1)).updateName(id, apiPlane.getName());
    }

    @Test
    void updatePlane_PlaneNotFound() {
        UUID id = UUID.randomUUID();
        when(planeService.updateName(id, "Jar Jar Binks")).thenThrow(new PlaneNotFoundException());
        assertThrows(PlaneNotFoundException.class, () -> planeFacade.updateName(id, "Jar Jar Binks"));
    }

    @Test
    void getPlanesByType(){
        var id1 = new UUID(0x1, 0xf);
        var id2 = new UUID(0x1, 0xe);
        cz.muni.fi.pa165.api.plane.Plane apiPlane1 = TestApiFactory.getPlaneEntity();
        apiPlane1.setId(id1);
        cz.muni.fi.pa165.api.plane.Plane apiPlane2 = TestApiFactory.getPlaneEntity();
        apiPlane2.setId(id2);

        cz.muni.fi.pa165.planes.dao.Plane daoPlane1 = TestDaoFactory.getPlaneEntity();
        daoPlane1.setId(id1);
        cz.muni.fi.pa165.planes.dao.Plane daoPlane2 = TestDaoFactory.getPlaneEntity();
        daoPlane2.setId(id2);
        when(planeService.getByType(daoPlane1.getPlaneType())).thenReturn(List.of(daoPlane1, daoPlane2));
        var result = planeFacade.getByType(apiPlane1.getPlaneType());
        assertEquals(result, List.of(apiPlane1, apiPlane2));
    }
}
