package cz.muni.fi.pa165.hr.integration;

import cz.muni.fi.pa165.api.employee.Employee;
import cz.muni.fi.pa165.hr.Application;
import cz.muni.fi.pa165.hr.facade.EmployeeFacade;
import cz.muni.fi.pa165.hr.repository.EmployeeRepository;
import cz.muni.fi.pa165.hr.repository.PilotRepository;
import cz.muni.fi.pa165.hr.repository.StewardRepository;
import cz.muni.fi.pa165.hr.rest.EmployeeController;
import cz.muni.fi.pa165.hr.service.EmployeeService;
import cz.muni.fi.pa165.hr.util.ObjectConverter;
import cz.muni.fi.pa165.hr.util.TestApiFactory;
import cz.muni.fi.pa165.hr.util.TestDaoFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {EmployeeService.class, EmployeeController.class})
//@SpringBootTest(classes = {Application.class})
@AutoConfigureMockMvc
public class EmployeeIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeRepository employeeRepository;

    @MockBean
    private EmployeeFacade employeeFacade;


    @MockBean
    private PilotRepository pilotRepository;

    @MockBean
    private StewardRepository stewardRepository;

//    @Test
    void findById_employeeFound_returnsEmployee() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        var employee = TestDaoFactory.getEmployeeEntity();
//        Mockito.when(employeeRScreenshot from 2024-04-30 19-09-51epository.findById(id)).thenReturn(Optional.of(employee));
        Mockito.when(employeeFacade.get(id)).thenReturn(TestApiFactory.getEmployeeEntity());

        String responseJson = mockMvc.perform(get("/employee/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
//        String responseJson = mockMvc.perform(get("/employee/{id}", id)
//                .accept(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andReturn()
//                .getResponse()
//                .getContentAsString(StandardCharsets.UTF_8);
//        Employee response = ObjectConverter.convertJsonToObject(responseJson, Employee.class);

        assertThat(responseJson).isEqualTo("{\"id\":\"00000000-0000-0001-0000-00000000000f\",\"name\":\"Name\",\"surname\":\"Surname\",\"gender\":true,\"dateOfBirth\":\"1980-05-15\",\"hired\":\"2010-05-15\",\"terminated\":null}");
    }
}
