package cz.muni.fi.pa165.hr.facade;

import cz.muni.fi.pa165.api.employee.requests.EmployeeRequest;
import cz.muni.fi.pa165.api.employee.requests.PilotRequest;
import cz.muni.fi.pa165.api.employee.requests.StewardRequest;
import cz.muni.fi.pa165.hr.dao.*;
import cz.muni.fi.pa165.hr.excdption.EmployeeNotFoundException;
import cz.muni.fi.pa165.hr.service.EmployeeService;
import cz.muni.fi.pa165.hr.util.TestApiFactory;
import cz.muni.fi.pa165.hr.util.TestDaoFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmployeeFacadeTest {

    @Mock
    private EmployeeService employeeService;

    @InjectMocks
    private EmployeeFacade employeeFacade;

    @BeforeEach
    public void simulate_post_construct() {
        employeeFacade.init();
    }

    @Test
    void createSteward() {
        cz.muni.fi.pa165.api.employee.Steward steward = TestApiFactory.getStewardEntity();
        Steward daoSteward = TestDaoFactory.getStewardEntity();
        StewardRequest stewardRequest = TestApiFactory.getStewardRequest();

        Mockito.when(employeeService.createSteward(daoSteward)).thenReturn(daoSteward);
        assertEquals(steward, employeeFacade.createSteward(stewardRequest));
        Mockito.verify(employeeService, Mockito.times(1)).createSteward(daoSteward);
    }

    @Test
    void createPilot() {
        cz.muni.fi.pa165.api.employee.Pilot pilot = TestApiFactory.getPilotEntity();
        pilot.setHired(null);
        pilot.setDateOfBirth(null);
        Pilot daoPilot = TestDaoFactory.getPilotEntity();
        PilotRequest pilotRequest = TestApiFactory.getPilotRequest();

        Mockito.when(employeeService.createPilot(daoPilot)).thenReturn(daoPilot);
        assertEquals(pilot, employeeFacade.createPilot(pilotRequest));
        Mockito.verify(employeeService, Mockito.times(1)).createPilot(daoPilot);
    }

    @Test
    void getById_employeeFound_returnsEmployee() {

        UUID id = new UUID(0x1, 0xf);
        Employee daoEmployee = TestDaoFactory.getEmployeeEntity();
        when(employeeService.get(id)).thenReturn(daoEmployee);

        cz.muni.fi.pa165.api.employee.Employee result = employeeFacade.get(id);

        assertNotNull(result);

        verify(employeeService, times(1)).get(id);
    }

    @Test
    void getById_employeeNotFound_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();
        when(employeeService.get(id)).thenThrow(new EmployeeNotFoundException());
        assertThrows(EmployeeNotFoundException.class, () -> employeeFacade.get(id));
    }

    @Test
    void updateEmployee_employeeFound() {
        UUID id = new UUID(0x1, 0xf);

        Employee update = new Employee();
        update.setGender(false);
        update.setName("Darth Vader");
        Mockito.when(employeeService.updateEmployee(id, update)).thenReturn(update);

        EmployeeRequest employeeRequest= new EmployeeRequest();
        employeeRequest.setGender(false);
        employeeRequest.setName("Darth Vader");
        employeeFacade.updateEmployee(id, employeeRequest);

        verify(employeeService, times(1)).updateEmployee(id, update);
    }

    @Test
    void updatePilot() {
        UUID id = new UUID(0x1, 0xf);

        Pilot update = new Pilot();
        update.setGender(false);
        update.setName("Darth Vader");
        PilotLicence licence = new PilotLicence();
        licence.setType(PlaneType.AIRBUS_A310);
        update.setLicences(Stream.of(licence)
                .collect(Collectors.toCollection(HashSet::new)));
        Mockito.when(employeeService.updatePilot(id, update)).thenReturn(update);

        PilotRequest pilotRequest= new PilotRequest();
        pilotRequest.setGender(false);
        pilotRequest.setName("Darth Vader");
        cz.muni.fi.pa165.api.plane.PlaneType planeType = cz.muni.fi.pa165.api.plane.PlaneType.AIRBUS_A310;
        pilotRequest.setLicences(Stream.of(planeType)
                .collect(Collectors.toCollection(HashSet::new)));
        employeeFacade.updatePilot(id, pilotRequest);

        verify(employeeService, times(1)).updatePilot(id, update);
    }

    @Test
    void updateSteward() {
        UUID id = UUID.randomUUID();

        Steward update = new Steward();
        update.setGender(false);
        update.setName("Darth Vader");
        Mockito.when(employeeService.updateSteward(id, update)).thenReturn(update);

        StewardRequest stewardRequest = new StewardRequest();
        stewardRequest.setGender(false);
        stewardRequest.setName("Darth Vader");
        employeeFacade.updateSteward(id, stewardRequest);

        verify(employeeService, times(1)).updateSteward(id, update);
    }

    @Test
    void terminate() {
        var id = new UUID(0x1, 0xf);
        var employee = new cz.muni.fi.pa165.api.employee.Employee();
        employee.setName("Name");
        employee.setSurname("Surname");
        employee.setGender(true);
        employee.setDateOfBirth(LocalDate.of(1980, 5, 15));
        employee.setId(id);

        var employeeDao = new Employee();
        employeeDao.setName("Name");
        employeeDao.setSurname("Surname");
        employeeDao.setGender(true);
        employeeDao.setDateOfBirth(LocalDate.of(1980, 5, 15));
        employeeDao.setId(id);
        System.out.println(employee);
        System.out.println(employeeDao);

        Mockito.when(employeeService.terminate(id)).thenReturn(employeeDao);

        assertEquals(employee, employeeFacade.terminate(id));

        verify(employeeService, times(1)).terminate(id);

    }

    @Test
    void getByLicence() {
        Pilot pilot1 = TestDaoFactory.getPilotEntity();
        pilot1.getLicences().forEach(licence -> licence.setId(UUID.randomUUID()));
        Set<Pilot> pilotsSet = Set.of(pilot1);
        // Arrange
        Mockito.when(employeeService.getByLicense(PlaneType.AIRBUS_A380)).thenReturn(pilotsSet);

        var pilot1Api = TestApiFactory.getPilotEntity();
        pilot1Api.setId(pilot1.getId());

        var pilotsSetApi = Set.of(pilot1Api);
        // Act
        var found = employeeFacade.getByLicence(cz.muni.fi.pa165.api.plane.PlaneType.AIRBUS_A380);

        // Assert
        assertEquals(pilotsSetApi,found);
    }
}