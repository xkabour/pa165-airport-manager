package cz.muni.fi.pa165.hr.rest;

import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.employee.requests.PilotRequest;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.hr.excdption.EmployeeNotFoundException;
import cz.muni.fi.pa165.hr.facade.EmployeeFacade;
import cz.muni.fi.pa165.hr.util.ObjectConverter;
import cz.muni.fi.pa165.hr.util.TestApiFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {PilotController.class})
class PilotControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeFacade employeeFacade;

    @Test
    void updatePilot_pilotFound_returnsPilot() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        Pilot updatedPilot = TestApiFactory.getPilotEntity();
        when(employeeFacade.updatePilot(Mockito.eq(id), Mockito.any(PilotRequest.class))).thenReturn(updatedPilot);

        String requestJson = "{\"id\":\"" + id + "\",\"name\":\"UpdatedName\",\"surname\":\"UpdatedSurname\",\"gender\":false,\"dateOfBirth\":\"1990-01-01\",\"hired\":\"2015-01-01\",\"terminated\":\"2022-01-01\",\"licences\":[\"CESSNA\"]}";
        String responseJson = mockMvc.perform(put("/employee/pilot/{id}", id).contentType(MediaType.APPLICATION_JSON).content(requestJson)).andExpect(status().isOk()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        Pilot response = ObjectConverter.convertJsonToObject(responseJson, Pilot.class);

        assertThat(response).isEqualTo(updatedPilot);
        Mockito.verify(employeeFacade, Mockito.times(1)).updatePilot(Mockito.eq(id), Mockito.any(PilotRequest.class));
    }

    @Test
    void updatePilot_pilotNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        when(employeeFacade.updatePilot(Mockito.eq(id), Mockito.any(PilotRequest.class))).thenThrow(new EmployeeNotFoundException());

        String requestJson = "{\"id\":\"" + id + "\",\"name\":\"UpdatedName\",\"surname\":\"UpdatedSurname\",\"gender\":false,\"dateOfBirth\":\"1990-01-01\",\"hired\":\"2015-01-01\",\"terminated\":\"2022-01-01\",\"licences\":[\"CESSNA\"]}";
        mockMvc.perform(put("/employee/pilot/{id}", id).contentType(MediaType.APPLICATION_JSON).content(requestJson)).andExpect(status().isNotFound());

        Mockito.verify(employeeFacade, Mockito.times(1)).updatePilot(Mockito.eq(id), Mockito.any(PilotRequest.class));
    }

    @Test
    void getByLicence_returnsPilots() throws Exception {
        PlaneType planeType = PlaneType.AIRBUS_A380;
        Set<Pilot> pilots = new HashSet<>();
        Pilot pilot = TestApiFactory.getPilotEntity();
        Pilot pilot2 = TestApiFactory.getPilotEntity();
        pilot2.setLicences(Collections.singleton(PlaneType.AIRBUS_A380));
        pilots.add(pilot);
        pilots.add(pilot2);

        Mockito.when(employeeFacade.getByLicence(planeType)).thenReturn(pilots);

        mockMvc.perform(get("/employee/pilot/licenced").param("planeType", planeType.toString()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}