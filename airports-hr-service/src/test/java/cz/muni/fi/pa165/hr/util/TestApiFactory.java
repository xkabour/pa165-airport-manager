package cz.muni.fi.pa165.hr.util;

import cz.muni.fi.pa165.api.employee.Employee;
import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.employee.Steward;
import cz.muni.fi.pa165.api.employee.requests.EmployeeRequest;
import cz.muni.fi.pa165.api.employee.requests.PilotRequest;
import cz.muni.fi.pa165.api.employee.requests.StewardRequest;
import cz.muni.fi.pa165.api.plane.PlaneType;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class TestApiFactory {


    public static Employee getEmployeeEntity() {
        Employee employee = new Employee();
        employee.setName("Name");
        employee.setSurname("Surname");
        employee.setGender(true);
        employee.setHired(LocalDate.of(2010, 5, 15));
        employee.setDateOfBirth(LocalDate.of(1980, 5, 15));

        employee.setId(new UUID(0x1, 0xf));

        return employee;
    }

    public static Pilot getPilotEntity() {
        Pilot pilot = new Pilot();
        pilot.setName("Name");
        pilot.setSurname("Surname");
        pilot.setGender(true);
        pilot.setHired(LocalDate.of(2010, 5, 15));
        pilot.setDateOfBirth(LocalDate.of(1980, 5, 15));

        var licence1 = PlaneType.AIRBUS_A380;
        var licence2 = PlaneType.AIRBUS_A350;

        pilot.setLicences(Stream.of(licence1, licence2).collect(Collectors.toCollection(HashSet::new)));
        pilot.setId(new UUID(0x1, 0xf));

        return pilot;
    }

    public static Steward getStewardEntity() {
        Steward steward = new Steward();
        steward.setName("Anakin");
        steward.setSurname("Skywalker");
        steward.setGender(true);
        steward.setHired(LocalDate.of(2010, 5, 15));
        steward.setDateOfBirth(LocalDate.of(1980, 5, 15));

        String language1 = "English";
        String language2 = "German";

        steward.setLanguages(Stream.of(language1, language2).collect(Collectors.toCollection(HashSet::new)));
        steward.setId(new UUID(0x1, 0xf));

        return steward;
    }

    public static PilotRequest getPilotRequest() {
        PilotRequest pilot = new PilotRequest();
        pilot.setName("Name");
        pilot.setSurname("Surname");
        pilot.setGender(true);
        pilot.setDateOfBirth(LocalDate.of(1980, 5, 15));

        var planeType1 = PlaneType.AIRBUS_A380;
        var planeType2 = PlaneType.AIRBUS_A350;

        pilot.setLicences(Stream.of(planeType2, planeType1).collect(Collectors.toCollection(HashSet::new)));

        return pilot;
    }

    public static EmployeeRequest getEmployeeRequest() {
        EmployeeRequest employee = new EmployeeRequest();
        employee.setName("Name");
        employee.setSurname("Surname");
        employee.setGender(true);
        employee.setDateOfBirth(LocalDate.of(1980, 5, 15));

        return employee;
    }


    public static StewardRequest getStewardRequest () {
        StewardRequest steward = new StewardRequest();
        steward.setName("Anakin");
        steward.setSurname("Skywalker");
        steward.setGender(true);
        steward.setDateOfBirth(LocalDate.of(1980, 5, 15));

        String language1 = "English";
        String language2 = "German";
        steward.setLanguages(Stream.of(language1, language2).toList());
        return steward;
    }
}
