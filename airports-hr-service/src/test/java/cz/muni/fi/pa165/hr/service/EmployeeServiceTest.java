package cz.muni.fi.pa165.hr.service;

import cz.muni.fi.pa165.hr.dao.*;
import cz.muni.fi.pa165.hr.excdption.EmployeeNotFoundException;
import cz.muni.fi.pa165.hr.repository.EmployeeRepository;
import cz.muni.fi.pa165.hr.repository.PilotRepository;
import cz.muni.fi.pa165.hr.repository.StewardRepository;
import cz.muni.fi.pa165.hr.util.TestDaoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private PilotRepository pilotRepository;

    @Mock
    private StewardRepository stewardRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @Test
    void createSteward() {
        Steward steward = TestDaoFactory.getStewardEntity();
        Mockito.when(stewardRepository.save(steward)).thenReturn(steward);
        assertEquals(steward, employeeService.createSteward(steward));
        Mockito.verify(stewardRepository, Mockito.times(1)).save(steward);
    }

    @Test
    void createPilot() {
        Pilot pilot = TestDaoFactory.getPilotEntity();
        Mockito.when(pilotRepository.save(pilot)).thenReturn(pilot);
        assertEquals(pilot, employeeService.createPilot(pilot));
        Mockito.verify(pilotRepository, Mockito.times(1)).save(pilot);
    }

        @Test
        void getById_employeeFound_returnsEmployee() {
            UUID id = new UUID(0x1, 0xf);
            var employee = TestDaoFactory.getEmployeeEntity();
            // Arrange
            Mockito.when(employeeRepository.findById(id)).thenReturn(Optional.of(employee));

            // Act
            Employee foundEntity = employeeService.get(id);

            // Assert
            assertThat(foundEntity).isEqualTo(employee);
        }

    @Test
    void getById_employeeNotFound_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();

        assertThrows(EmployeeNotFoundException.class, () -> Optional
                .ofNullable(employeeService.get(id))
                .orElseThrow(EmployeeNotFoundException::new));
    }

    @Test
    void updateEmployee_employeeFound() {
        UUID id = new UUID(0x1, 0xf);
        Employee existingEmployee = TestDaoFactory.getEmployeeEntity();

        Employee update = new Employee();
        update.setGender(false);
        update.setName("Darth Vader");

        Employee updatedEmployee = TestDaoFactory.getEmployeeEntity();
        updatedEmployee.setId(id);
        updatedEmployee.setGender(false);
        updatedEmployee.setName("Darth Vader");

        Mockito.when(employeeRepository.findById(id)).thenReturn(Optional.of(existingEmployee));
        Mockito.when(employeeRepository.save(Mockito.any(Employee.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertEquals(updatedEmployee, employeeService.updateEmployee(id, update));
        Mockito.verify(employeeRepository, Mockito.times(1)).save(updatedEmployee);
    }

    @Test
    void updateEmployee_employeeNotFound_throwsEmployeeNotFoundException() {
        UUID id = UUID.randomUUID();
        Employee update = new Employee();

        assertThrows(EmployeeNotFoundException.class, () -> Optional
                .ofNullable(employeeService.updateEmployee(id, update))
                .orElseThrow(EmployeeNotFoundException::new));

        Mockito.verify(employeeRepository, Mockito.times(0)).save(Mockito.any());
    }


    @Test
    void updateSteward_stewardFound() {
        UUID id = new UUID(0x1, 0xf);
        Steward existingSteward = TestDaoFactory.getStewardEntity();

        Steward update = new Steward();
        update.setGender(false);
        update.setName("Darth Vader");
        Language language = new Language();
        language.setLanguage("Klingon");
        update.setLanguages(Stream.of(language)
                .collect(Collectors.toCollection(HashSet::new)));

        Steward updatedSteward = TestDaoFactory.getStewardEntity();
        updatedSteward.setId(id);
        updatedSteward.setGender(false);
        updatedSteward.setName("Darth Vader");
        updatedSteward.getLanguages().add(language);

        Mockito.when(stewardRepository.findById(id)).thenReturn(Optional.of(existingSteward));
        Mockito.when(stewardRepository.save(Mockito.any(Steward.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertEquals(updatedSteward, employeeService.updateSteward(id, update));
        Mockito.verify(stewardRepository, Mockito.times(1)).save(updatedSteward);
    }

    @Test
    void updateSteward_stewardNotFound_throwsEmployeeNotFoundException() {
        UUID id = UUID.randomUUID();
        Steward update = new Steward();

        assertThrows(EmployeeNotFoundException.class, () -> Optional
                .ofNullable(employeeService.updateSteward(id, update))
                .orElseThrow(EmployeeNotFoundException::new));

        Mockito.verify(stewardRepository, Mockito.times(0)).save(Mockito.any());
    }

    @Test
    void updatePilot_pilotFound() {
        UUID id = new UUID(0x1, 0xf);
        Pilot existingPilot = TestDaoFactory.getPilotEntity();

        Pilot update = new Pilot();
        update.setGender(false);
        update.setName("Darth Vader");
        PilotLicence licence = new PilotLicence();
        licence.setType(PlaneType.AIRBUS_A310);
        update.setLicences(Stream.of(licence)
                .collect(Collectors.toCollection(HashSet::new)));

        Pilot updatedPilot = TestDaoFactory.getPilotEntity();
        updatedPilot.setId(id);
        updatedPilot.setGender(false);
        updatedPilot.setName("Darth Vader");
        updatedPilot.setLicences(
                Stream.concat(existingPilot.getLicences().stream(),update.getLicences().stream())
                        .collect(Collectors.toSet()));

        Mockito.when(pilotRepository.findById(id)).thenReturn(Optional.of(existingPilot));
        Mockito.when(pilotRepository.save(Mockito.any(Pilot.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertEquals(updatedPilot, employeeService.updatePilot(id, update));
        Mockito.verify(pilotRepository, Mockito.times(1)).save(updatedPilot);
    }

    @Test
    void updatePilot_pilotNotFound_throwsEmployeeNotFoundException() {
        UUID id = UUID.randomUUID();
        Pilot update = new Pilot();

        assertThrows(EmployeeNotFoundException.class, () -> Optional
                .ofNullable(employeeService.updatePilot(id, update))
                .orElseThrow(EmployeeNotFoundException::new));

        Mockito.verify(pilotRepository, Mockito.times(0)).save(Mockito.any());
    }

    @Test
    void terminateEmployee_employeeFound() {
        UUID id = new UUID(0x1, 0xf);
        Employee existingEmployee = TestDaoFactory.getEmployeeEntity();
        Mockito.when(employeeRepository.findById(id)).thenReturn(Optional.of(existingEmployee));
        Mockito.when(employeeRepository.save(Mockito.any(Employee.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertNotNull(employeeService.terminate(id).getTerminated());
        Mockito.verify(employeeRepository, Mockito.times(1)).findById(id);
        Mockito.verify(employeeRepository, Mockito.times(1)).save(Mockito.any());

    }

    @Test
    void terminateEmployee_employeeNotFound_throwsEmployeeNotFoundException() {
        UUID id = UUID.randomUUID();

        assertThrows(EmployeeNotFoundException.class, () -> Optional
                .ofNullable(employeeService.terminate(id))
                .orElseThrow(EmployeeNotFoundException::new));

        Mockito.verify(employeeRepository, Mockito.times(0)).save(Mockito.any());
    }

    @Test
    void findByLicense() {
        Pilot pilot1 = TestDaoFactory.getPilotEntity();
        Pilot pilot2 = TestDaoFactory.getPilotEntity();
        pilot2.setId(UUID.randomUUID());
        pilot1.getLicences().forEach(licence -> licence.setId(UUID.randomUUID()));
        pilot2.getLicences().forEach(licence -> licence.setId(UUID.randomUUID()));
        Set<Pilot> pilotsSet = Set.of(pilot1, pilot2);
        // Arrange
        Mockito.when(pilotRepository.findByLicence(PlaneType.AIRBUS_A380)).thenReturn(pilotsSet);

        // Act
        Set<Pilot> foundEntity = employeeService.getByLicense(PlaneType.AIRBUS_A380);

        // Assert
        assertThat(foundEntity).isEqualTo(pilotsSet);
    }
}