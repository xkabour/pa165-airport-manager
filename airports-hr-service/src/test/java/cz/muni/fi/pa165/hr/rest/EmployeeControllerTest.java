package cz.muni.fi.pa165.hr.rest;

import cz.muni.fi.pa165.api.employee.Employee;
import cz.muni.fi.pa165.api.employee.requests.EmployeeRequest;
import cz.muni.fi.pa165.hr.excdption.EmployeeNotFoundException;
import cz.muni.fi.pa165.hr.facade.EmployeeFacade;
import cz.muni.fi.pa165.hr.util.ObjectConverter;
import cz.muni.fi.pa165.hr.util.TestApiFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {EmployeeController.class})
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeFacade employeeFacade;

    @Test
    void findById_employeeFound_returnsEmployee() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        Mockito.when(employeeFacade.get(id)).thenReturn(TestApiFactory.getEmployeeEntity());

        String responseJson = mockMvc.perform(get("/employee/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Employee response = ObjectConverter.convertJsonToObject(responseJson, Employee.class);

        assertThat(responseJson).isEqualTo("{\"id\":\"00000000-0000-0001-0000-00000000000f\",\"name\":\"Name\",\"surname\":\"Surname\",\"gender\":true,\"dateOfBirth\":\"1980-05-15\",\"hired\":\"2010-05-15\",\"terminated\":null}");
        assertThat(response).isEqualTo(TestApiFactory.getEmployeeEntity());
    }

    @Test
    void findById_employeeNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(employeeFacade.get(id)).thenThrow(new EmployeeNotFoundException());

        mockMvc.perform(get("/employee/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        Mockito.verify(employeeFacade, Mockito.times(1)).get(id);
    }

    @Test
    void updateEmployee_employeeFound_returnsEmployee() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        Employee updatedEmployee = TestApiFactory.getEmployeeEntity();
        Mockito.when(employeeFacade.updateEmployee(Mockito.eq(id), Mockito.any(EmployeeRequest.class))).thenReturn(updatedEmployee);

        String requestJson = "{\"id\":\"" + id + "\",\"name\":\"UpdatedName\",\"surname\":\"UpdatedSurname\",\"gender\":false,\"dateOfBirth\":\"1990-01-01\",\"hired\":\"2015-01-01\",\"terminated\":\"2022-01-01\"}";
        String responseJson = mockMvc.perform(put("/employee/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Employee response = ObjectConverter.convertJsonToObject(responseJson, Employee.class);
        assertThat(response).isEqualTo(updatedEmployee);
        Mockito.verify(employeeFacade, Mockito.times(1)).updateEmployee(Mockito.eq(id), Mockito.any(EmployeeRequest.class));
    }

    @Test
    void updateEmployee_employeeNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(employeeFacade.updateEmployee(Mockito.eq(id), Mockito.any(EmployeeRequest.class))).thenThrow(new EmployeeNotFoundException());

        mockMvc.perform(put("/employee/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"" + id + "\",\"name\":\"UpdatedName\",\"surname\":\"UpdatedSurname\",\"gender\":false,\"dateOfBirth\":\"1990-01-01\",\"hired\":\"2015-01-01\",\"terminated\":\"2022-01-01\"}"))
                .andExpect(status().isNotFound());

        Mockito.verify(employeeFacade, Mockito.times(1)).updateEmployee(Mockito.eq(id), Mockito.any(EmployeeRequest.class));
    }

    @Test
    void terminateEmployee_employeeFound() throws Exception {
        UUID id = UUID.randomUUID();
        Employee terminatedEmployee = TestApiFactory.getEmployeeEntity();
        LocalDate date = LocalDate.now();
        terminatedEmployee.setTerminated(date);
        Mockito.when(employeeFacade.terminate(id)).thenReturn(terminatedEmployee);

        mockMvc.perform(delete("/employee/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.terminated").exists())
                .andExpect(jsonPath("$.terminated").value(date.toString()));

        Mockito.verify(employeeFacade, Mockito.times(1)).terminate(id);
    }

    @Test
    void terminateEmployee_employeeNotFound_404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(employeeFacade.terminate(id)).thenThrow(new EmployeeNotFoundException());

        mockMvc.perform(delete("/employee/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());


        Mockito.verify(employeeFacade, Mockito.times(1)).terminate(id);
    }
}