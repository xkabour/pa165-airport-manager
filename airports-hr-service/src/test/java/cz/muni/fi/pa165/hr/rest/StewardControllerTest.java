package cz.muni.fi.pa165.hr.rest;

import cz.muni.fi.pa165.api.employee.Steward;
import cz.muni.fi.pa165.api.employee.requests.StewardRequest;
import cz.muni.fi.pa165.hr.excdption.EmployeeNotFoundException;
import cz.muni.fi.pa165.hr.facade.EmployeeFacade;
import cz.muni.fi.pa165.hr.util.ObjectConverter;
import cz.muni.fi.pa165.hr.util.TestApiFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {StewardController.class})
class StewardControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeFacade employeeFacade;

    @Test
    void updateSteward_stewardFound_returnsSteward() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        Steward updatedSteward = TestApiFactory.getStewardEntity();
        when(employeeFacade.updateSteward(Mockito.eq(id), Mockito.any(StewardRequest.class))).thenReturn(updatedSteward);

        String requestJson = "{\"id\":\"" + id + "\",\"name\":\"UpdatedName\",\"surname\":\"UpdatedSurname\",\"gender\":false,\"dateOfBirth\":\"1990-01-01\",\"hired\":\"2015-01-01\",\"terminated\":\"2022-01-01\",\"languages\":[\"english\"]}";
        String responseJson = mockMvc.perform(put("/employee/steward/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Steward response = ObjectConverter.convertJsonToObject(responseJson, Steward.class);

        assertThat(response).isEqualTo(updatedSteward);
        Mockito.verify(employeeFacade, Mockito.times(1)).updateSteward(Mockito.eq(id), Mockito.any(StewardRequest.class));
    }

    @Test
    void updateSteward_stewardNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        when(employeeFacade.updateSteward(Mockito.eq(id), Mockito.any(StewardRequest.class))).thenThrow(new EmployeeNotFoundException());

        String requestJson = "{\"id\":\"" + id + "\",\"name\":\"UpdatedName\",\"surname\":\"UpdatedSurname\",\"gender\":false,\"dateOfBirth\":\"1990-01-01\",\"hired\":\"2015-01-01\",\"terminated\":\"2022-01-01\",\"languages\":[\"english\"]}";
        mockMvc.perform(put("/employee/steward/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isNotFound());

        Mockito.verify(employeeFacade, Mockito.times(1)).updateSteward((Mockito.eq(id)), Mockito.any(StewardRequest.class));
    }
}