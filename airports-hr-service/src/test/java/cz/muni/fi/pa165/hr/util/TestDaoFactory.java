package cz.muni.fi.pa165.hr.util;

import cz.muni.fi.pa165.hr.dao.*;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TestDaoFactory {

    public static Pilot getPilotEntity() {
        Pilot pilot = new Pilot();
        pilot.setName("Name");
        pilot.setSurname("Surname");
        pilot.setGender(true);
       // pilot.setHired(LocalDate.of(2010, 5, 15));

        PilotLicence licence1 = new PilotLicence();
        PilotLicence licence2 = new PilotLicence();
        licence1.setType(PlaneType.AIRBUS_A380);
        licence2.setType(PlaneType.AIRBUS_A350);

        pilot.setLicences(Stream.of(licence1, licence2)
                .collect(Collectors.toCollection(HashSet::new)));
        pilot.setId(new UUID(0x1, 0xf));

        return pilot;
    }

    public static Employee getEmployeeEntity() {
        Employee employee = new Employee();
        employee.setName("Name");
        employee.setSurname("Surname");
        employee.setGender(true);
        employee.setHired(LocalDate.of(2010, 5, 15));
        employee.setDateOfBirth(LocalDate.of(1980, 5, 15));

        employee.setId(new UUID(0x1, 0xf));

        return employee;
    }

    public static Steward getStewardEntity() {
        Steward steward = new Steward();
        steward.setName("Anakin");
        steward.setSurname("Skywalker");
        steward.setGender(true);
        steward.setHired(LocalDate.of(2010, 5, 15));
        steward.setDateOfBirth(LocalDate.of(1980, 5, 15));

        Language language1 = new Language();
        Language language2 = new Language();
        language1.setLanguage("English");
        language2.setLanguage("German");

        steward.setLanguages(Stream.of(language1, language2)
                .collect(Collectors.toCollection(HashSet::new)));
        steward.setId(new UUID(0x1, 0xf));

        return steward;
    }
}
