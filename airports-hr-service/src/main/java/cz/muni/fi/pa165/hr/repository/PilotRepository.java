package cz.muni.fi.pa165.hr.repository;

import cz.muni.fi.pa165.hr.dao.Pilot;
import cz.muni.fi.pa165.hr.dao.PlaneType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface PilotRepository extends CrudRepository<Pilot, UUID> {

    @Query("SELECT DISTINCT p FROM Pilot p JOIN FETCH p.licences l WHERE l.type = :planeType")
    Set<Pilot> findByLicence(PlaneType planeType);
}