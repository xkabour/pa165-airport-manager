package cz.muni.fi.pa165.hr.dao;

import cz.muni.fi.pa165.hr.Constants;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "employee", schema = Constants.HR_SCHEMA)
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String name;
    private String surname;
    private Boolean gender;

    private LocalDate dateOfBirth;

    private LocalDate hired;
    private LocalDate terminated;

}
