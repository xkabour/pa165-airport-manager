package cz.muni.fi.pa165.hr.dao;

import cz.muni.fi.pa165.hr.Constants;
import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "language", schema = Constants.HR_SCHEMA)
public class Language {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String language;
}
