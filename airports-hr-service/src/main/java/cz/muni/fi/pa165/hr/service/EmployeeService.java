package cz.muni.fi.pa165.hr.service;

import cz.muni.fi.pa165.hr.dao.PlaneType;
import cz.muni.fi.pa165.hr.dao.Employee;
import cz.muni.fi.pa165.hr.dao.Pilot;
import cz.muni.fi.pa165.hr.dao.Steward;
import cz.muni.fi.pa165.hr.repository.EmployeeRepository;
import cz.muni.fi.pa165.hr.repository.PilotRepository;
import cz.muni.fi.pa165.hr.repository.StewardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import cz.muni.fi.pa165.hr.excdption.EmployeeNotFoundException;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final PilotRepository pilotRepository;
    private final StewardRepository stewardRepository;

    public Steward createSteward(Steward steward) {
        return stewardRepository.save(steward);
    }

    public Pilot createPilot(Pilot pilot) {
        return pilotRepository.save(pilot);
    }

    public Employee get(UUID id) {
        var result = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        return result;
    }

    public Employee updateEmployee(UUID id, Employee employeeUpdate) {
        var employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee = updateEmployeeValues(employee, employeeUpdate);
        return employeeRepository.save(employee);
    }

    public Steward updateSteward(UUID id, Steward stewardUpdate) {
        var employee = stewardRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        updateEmployeeValues(employee, stewardUpdate);
        employee.getLanguages().addAll(stewardUpdate.getLanguages() != null ? stewardUpdate.getLanguages() : List.of());
        return stewardRepository.save(employee);
    }

    public Pilot updatePilot(UUID id, Pilot pilotUpdate) {
        var employee = pilotRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        updateEmployeeValues(employee, pilotUpdate);
        employee.getLicences().addAll(pilotUpdate.getLicences() != null ? pilotUpdate.getLicences() : List.of());
        return pilotRepository.save(employee);
    }

    public Employee terminate(UUID id) {
        var employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee.setTerminated(LocalDate.now());
        return employeeRepository.save(employee);
    }

    public Set<Pilot> getByLicense(PlaneType planeType) {
        return pilotRepository.findByLicence(planeType);
    }

    private Employee updateEmployeeValues(Employee original, Employee employeeUpdate){
        if (employeeUpdate.getName() != null){
            original.setName(employeeUpdate.getName());
        }

        if (employeeUpdate.getSurname() != null){
            original.setSurname(employeeUpdate.getSurname());
        }


        if (employeeUpdate.getHired() != null){
            original.setHired(employeeUpdate.getHired());
        }

        if (employeeUpdate.getDateOfBirth() != null){
            original.setDateOfBirth(employeeUpdate.getDateOfBirth());
        }

        if (employeeUpdate.getGender() != null){
            original.setGender(employeeUpdate.getGender());
        }
        return original;
    }
}