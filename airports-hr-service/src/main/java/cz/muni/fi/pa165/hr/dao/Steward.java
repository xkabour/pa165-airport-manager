package cz.muni.fi.pa165.hr.dao;

import cz.muni.fi.pa165.hr.Constants;

import javax.persistence.*;

import lombok.Data;

import java.util.Set;

@Data
@Entity
@Table(name = "stewards", schema = Constants.HR_SCHEMA)
@PrimaryKeyJoinColumn(name = "employee_id")
public class Steward extends Employee{

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Language> languages;
}
