package cz.muni.fi.pa165.hr.rest;


import cz.muni.fi.pa165.api.employee.Steward;
import cz.muni.fi.pa165.api.employee.requests.StewardRequest;
import cz.muni.fi.pa165.hr.facade.EmployeeFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class StewardController {

    private final EmployeeFacade employeeFacade;

    @PostMapping("/employee/steward")
    public Steward createSteward(@RequestBody StewardRequest request) {
        return employeeFacade.createSteward(request);
    }

    @PutMapping("/employee/steward/{id}")
    public Steward update(@PathVariable UUID id, @RequestBody StewardRequest request){
        return employeeFacade.updateSteward(id, request);
    }
}
