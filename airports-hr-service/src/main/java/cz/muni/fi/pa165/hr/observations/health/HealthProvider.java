package cz.muni.fi.pa165.hr.observations.health;

import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class HealthProvider {

    private AtomicInteger counter = new AtomicInteger(0);

    public boolean getSystemHealth() {
        return counter.getAndIncrement() % 2 == 0;
    }
}
