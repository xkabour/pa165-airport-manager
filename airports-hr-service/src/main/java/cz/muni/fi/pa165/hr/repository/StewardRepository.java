package cz.muni.fi.pa165.hr.repository;

import cz.muni.fi.pa165.hr.dao.Steward;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface StewardRepository extends CrudRepository<Steward, UUID> {
}