package cz.muni.fi.pa165.hr.facade;

import cz.muni.fi.pa165.api.employee.Employee;
import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.api.employee.Steward;
import cz.muni.fi.pa165.api.employee.requests.EmployeeRequest;
import cz.muni.fi.pa165.api.employee.requests.PilotRequest;
import cz.muni.fi.pa165.api.employee.requests.StewardRequest;
import cz.muni.fi.pa165.hr.dao.Language;
import cz.muni.fi.pa165.hr.dao.PilotLicence;
import cz.muni.fi.pa165.hr.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeFacade {
    private final EmployeeService employeeService;
    private final ModelMapper modelMapper = new ModelMapper();

    @PostConstruct
    public void init() {
        Converter<List<String>, Set<Language>> toLanguages =
                ctx -> ctx.getSource() == null
                        ? null
                        : ctx.getSource().stream()
                                .map(l -> {
                                    var lang = new Language();
                                    lang.setLanguage(l);
                                    return lang;
                                })
                                .collect(Collectors.toSet());
        modelMapper.createTypeMap(StewardRequest.class,  cz.muni.fi.pa165.hr.dao.Steward.class)
                .addMappings(mapper -> mapper.using(toLanguages).map(StewardRequest::getLanguages, cz.muni.fi.pa165.hr.dao.Steward::setLanguages));

        Converter<Set<Language>, Set<String>> toStrings =
                ctx -> ctx.getSource() == null
                        ? null
                        : ctx.getSource().stream().map(Language::getLanguage).collect(Collectors.toSet());
        modelMapper.createTypeMap(cz.muni.fi.pa165.hr.dao.Steward.class, Steward.class)
                .addMappings(mapper -> mapper.using(toStrings).map(cz.muni.fi.pa165.hr.dao.Steward::getLanguages, Steward::setLanguages));

        Converter<Set<PlaneType>, Set<PilotLicence>> toLicences =
                ctx -> ctx.getSource() == null
                        ? null
                        : ctx.getSource().stream()
                        .map(l -> {
                            System.out.println(l);
                            var lin = new PilotLicence();
                            lin.setType(switch (l){
                                case CESSNA -> cz.muni.fi.pa165.hr.dao.PlaneType.CESSNA;
                                case AIRBUS_A380 -> cz.muni.fi.pa165.hr.dao.PlaneType.AIRBUS_A380;
                                case AIRBUS_A350 -> cz.muni.fi.pa165.hr.dao.PlaneType.AIRBUS_A350;
                                case AIRBUS_A320 -> cz.muni.fi.pa165.hr.dao.PlaneType.AIRBUS_A320;
                                case AIRBUS_A310 -> cz.muni.fi.pa165.hr.dao.PlaneType.AIRBUS_A310;
                            });
                            return lin;
                        })
                        .collect(Collectors.toSet());
        modelMapper.createTypeMap(PilotRequest.class,  cz.muni.fi.pa165.hr.dao.Pilot.class)
                .addMappings(mapper -> mapper.using(toLicences).map(PilotRequest::getLicences, cz.muni.fi.pa165.hr.dao.Pilot::setLicences));

        Converter<Set<PilotLicence>, Set<PlaneType>> toPlaneTypes =
                ctx -> ctx.getSource() == null
                        ? null
                        : ctx.getSource().stream().map(l-> modelMapper.map(l.getType(), PlaneType.class)).collect(Collectors.toSet());
        modelMapper.createTypeMap(cz.muni.fi.pa165.hr.dao.Pilot.class, Pilot.class)
                .addMappings(mapper -> mapper.using(toPlaneTypes).map(cz.muni.fi.pa165.hr.dao.Pilot::getLicences, Pilot::setLicences));
    }

    public Steward createSteward(StewardRequest stewardRequest){
        var steward = modelMapper.map(stewardRequest, cz.muni.fi.pa165.hr.dao.Steward.class);
        var result = employeeService.createSteward(steward);
        return modelMapper.map(result, Steward.class);
    }

    public Pilot createPilot(PilotRequest pilotRequest){
        var pilot = modelMapper.map(pilotRequest, cz.muni.fi.pa165.hr.dao.Pilot.class);

        var result = employeeService.createPilot(pilot);
        return modelMapper.map(result, Pilot.class);
    }

    public Employee get(UUID id){
        cz.muni.fi.pa165.hr.dao.Employee result = employeeService.get(id);
        return modelMapper.map(result, Employee.class);
    }

    public Employee updateEmployee(UUID id, EmployeeRequest employeeRequest){
        var result = employeeService.updateEmployee(id, modelMapper.map(employeeRequest, cz.muni.fi.pa165.hr.dao.Employee.class));
        return modelMapper.map(result, Employee.class);
    }

    public Pilot updatePilot(UUID id, PilotRequest pilotRequest){
        var result = employeeService.updatePilot(id, modelMapper.map(pilotRequest, cz.muni.fi.pa165.hr.dao.Pilot.class));
        return modelMapper.map(result, Pilot.class);
    }

    public Steward updateSteward(UUID id, StewardRequest stewardRequest){
        var result = employeeService.updateSteward(id, modelMapper.map(stewardRequest, cz.muni.fi.pa165.hr.dao.Steward.class));
        return modelMapper.map(result, Steward.class);
    }

    public Employee terminate(UUID id){
        var result = employeeService.terminate(id);
        return modelMapper.map(result, Employee.class);
    }

    public Set<Pilot> getByLicence(PlaneType planeType){
        var result = employeeService.getByLicense(modelMapper.map(planeType, cz.muni.fi.pa165.hr.dao.PlaneType.class));
        return result.stream().map(p -> modelMapper.map(p, Pilot.class)).collect(Collectors.toSet());
    }
}
