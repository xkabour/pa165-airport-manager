package cz.muni.fi.pa165.hr.rest;

import cz.muni.fi.pa165.api.employee.requests.EmployeeRequest;
import cz.muni.fi.pa165.api.employee.Employee;
import cz.muni.fi.pa165.hr.facade.EmployeeFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeFacade employeeFacade;

    @GetMapping(path = "/employee/{id}")
    public Employee get(@PathVariable UUID id) {
        return employeeFacade.get(id);
    }

    @PutMapping("/employee/{id}")
    public Employee update(@PathVariable UUID id, @RequestBody EmployeeRequest request){
        return employeeFacade.updateEmployee(id, request);
    }

    @DeleteMapping("/employee/{id}")
    public Employee terminate(@PathVariable UUID id){
        return employeeFacade.terminate(id);
    }
}
