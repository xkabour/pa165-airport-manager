package cz.muni.fi.pa165.hr.rest;

import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.api.employee.requests.PilotRequest;
import cz.muni.fi.pa165.hr.facade.EmployeeFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class PilotController {

    private final EmployeeFacade employeeFacade;

    @PostMapping("/employee/pilot")
    public Pilot create(@RequestBody PilotRequest request) {
        return employeeFacade.createPilot(request);
    }

    @PutMapping("/employee/pilot/{id}")
    public Pilot update(@PathVariable UUID id, @RequestBody PilotRequest request){
        return employeeFacade.updatePilot(id, request);
    }

    @GetMapping("/employee/pilot/licenced")
    public Set<Pilot> getByLicence(@RequestParam PlaneType planeType){
        return employeeFacade.getByLicence(planeType);
    }

}
