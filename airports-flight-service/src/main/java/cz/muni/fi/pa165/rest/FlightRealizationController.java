package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.flight.FlightRealization;
import cz.muni.fi.pa165.api.flight.requests.FlightRealizationRequest;
import cz.muni.fi.pa165.exception.FlightRealizationNotFoundException;
import cz.muni.fi.pa165.facade.FlightRealizationFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/flightrealization")
public class FlightRealizationController {

    @Autowired
    private final FlightRealizationFacade flightRealizationFacade;

    @GetMapping
    public List<FlightRealization> getAllFlightRealizations() {
        return flightRealizationFacade.getAllFlightRealizations();
    }


    @GetMapping("/{id}")
    public FlightRealization getFlightRealizationById(@PathVariable UUID id) {
        return flightRealizationFacade.getFlightRealization(id);
    }

    @PostMapping
    public FlightRealization createFlightRealization(@RequestBody FlightRealizationRequest flightRealizationRequest) {
        return flightRealizationFacade.createFlightRealization(flightRealizationRequest);
    }

    @PutMapping("/{id}")
    public FlightRealization updateFlightRealization(@PathVariable UUID id, @RequestBody FlightRealizationRequest flightRealizationRequest) {
        return flightRealizationFacade.updateFlightRealization(id, flightRealizationRequest);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFlightRealization(@PathVariable UUID id) {
        flightRealizationFacade.deleteFlightRealization(id);
        return new ResponseEntity<>("Flight Realization deleted sucessfully", HttpStatus.OK);
    }

    @ExceptionHandler(FlightRealizationNotFoundException.class)
    public ResponseEntity<String> handleIssueReportNotFoundException(FlightRealizationNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Flight realization not found");
    }
}
