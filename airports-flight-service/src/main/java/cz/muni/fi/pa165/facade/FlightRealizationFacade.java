package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.flight.FlightRealization;
import cz.muni.fi.pa165.api.flight.requests.FlightRealizationRequest;
import cz.muni.fi.pa165.service.FlightRealizationService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FlightRealizationFacade {

    private final FlightRealizationService flightRealizationService;
    private final ModelMapper modelMapper = new ModelMapper();

    public List<FlightRealization> getAllFlightRealizations() {
        var result = flightRealizationService.getAllFlightRealizations();
        return result.stream().map(p -> modelMapper.map(p, FlightRealization.class)).toList();
    }

    public FlightRealization getFlightRealization(UUID id) {
        var result = flightRealizationService.getFlightRealizationById(id);
        return modelMapper.map(result, FlightRealization.class);
    }

    public FlightRealization createFlightRealization(FlightRealizationRequest flightRealizationRequest) {
        var result = flightRealizationService.createFlightRealization(modelMapper.map(flightRealizationRequest, cz.muni.fi.pa165.dao.FlightRealization.class));
        return modelMapper.map(result, FlightRealization.class);
    }


    public FlightRealization updateFlightRealization(UUID id, FlightRealizationRequest flightRealizationRequest) {
        var result = flightRealizationService.updateFlightRealization(id, modelMapper.map(flightRealizationRequest, cz.muni.fi.pa165.dao.FlightRealization.class));
        return modelMapper.map(result, FlightRealization.class);
    }


    public void deleteFlightRealization(UUID id) {
        flightRealizationService.deleteFlightRealization(id);
    }

}
