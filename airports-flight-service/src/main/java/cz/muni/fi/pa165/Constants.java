package cz.muni.fi.pa165;

public class Constants {
    public static final String FLIGHT_SCHEMA = "flight";
    public static final String FLIGHT_REALIZATION_SCHEMA = "flight";
    public static final String FLIGHT_ISSUE_REPORT_SCHEMA = "issuereport";
}
