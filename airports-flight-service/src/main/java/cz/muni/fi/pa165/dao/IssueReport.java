package cz.muni.fi.pa165.dao;

import cz.muni.fi.pa165.Constants;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Data
@Entity
@Table(name = "issue")
@Inheritance(strategy = InheritanceType.JOINED)
public class IssueReport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private UUID planeId;
    private UUID flightEncountered;
    private String description;
}
