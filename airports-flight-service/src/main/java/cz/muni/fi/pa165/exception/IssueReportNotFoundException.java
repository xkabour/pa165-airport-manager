package cz.muni.fi.pa165.exception;

public class IssueReportNotFoundException extends RuntimeException {

    public IssueReportNotFoundException() {
    }

    public IssueReportNotFoundException(String message) {
        super(message);
    }

    public IssueReportNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public IssueReportNotFoundException(Throwable cause) {
        super(cause);
    }

    public IssueReportNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
