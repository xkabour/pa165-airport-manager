package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.dao.FlightRealization;
import cz.muni.fi.pa165.exception.FlightRealizationNotFoundException;
import cz.muni.fi.pa165.repository.FlightRealizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class FlightRealizationService {

    @Autowired
    private FlightRealizationRepository flightRealizationRepository;

    public List<FlightRealization> getAllFlightRealizations() {
        return flightRealizationRepository.findAll();
    }

    public FlightRealization getFlightRealizationById(UUID id) {
        return flightRealizationRepository.findById(id).orElseThrow(FlightRealizationNotFoundException::new);
    }

    // Create a new flight realization
    public FlightRealization createFlightRealization(FlightRealization flightRealization) {
        return flightRealizationRepository.save(flightRealization);
    }

    // Update an existing flight realization
    public FlightRealization updateFlightRealization(UUID id, FlightRealization flightRealization) {
        FlightRealization existingFlightRealization = flightRealizationRepository.findById(id).orElseThrow(FlightRealizationNotFoundException::new);
        existingFlightRealization.setArrivalTime(flightRealization.getArrivalTime());
        existingFlightRealization.setDuration(flightRealization.getDuration());
        existingFlightRealization.setDepartureTime(flightRealization.getDepartureTime());
        existingFlightRealization.setKilometersFlown(flightRealization.getKilometersFlown());
        existingFlightRealization.setReport(flightRealization.getReport());

        FlightRealization updateFlightRealization = flightRealizationRepository.save(existingFlightRealization);

        return updateFlightRealization;
    }

    public void deleteFlightRealization(UUID id) {
        flightRealizationRepository.deleteById(id);
    }
}


