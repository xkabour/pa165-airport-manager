package cz.muni.fi.pa165.rest;


import cz.muni.fi.pa165.api.flight.IssueReport;
import cz.muni.fi.pa165.api.flight.requests.IssueReportRequest;
import cz.muni.fi.pa165.exception.IssueReportNotFoundException;
import cz.muni.fi.pa165.facade.IssueReportFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/issuereports")
public class IssueReportController {

    @Autowired
    private final IssueReportFacade issueReportFacade;

    @GetMapping
    public List<IssueReport> getAllIssueReports() {
        return issueReportFacade.getAllIssueReports();
    }

    @GetMapping("/{id}")
    public IssueReport getIssueReportById(@PathVariable UUID id) {
        return issueReportFacade.getIssueReport(id);
    }

    @PostMapping
    public IssueReport createIssueReport(@RequestBody IssueReportRequest issueReportRequest) {
        return issueReportFacade.createIssueReport(issueReportRequest);
    }

    @PutMapping("/{id}")
    public IssueReport updateIssueReport(@PathVariable UUID id, @RequestBody IssueReportRequest issueReportRequest) {
        return issueReportFacade.updateIssueReport(id, issueReportRequest);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteIssueReport(@PathVariable UUID id) {
        issueReportFacade.deleteIssueReport(id);
        return new ResponseEntity<>("Issue deleted sucessfully", HttpStatus.OK);
    }

    @ExceptionHandler(IssueReportNotFoundException.class)
    public ResponseEntity<String> handleIssueReportNotFoundException(IssueReportNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Issue Report not found");
    }
}
