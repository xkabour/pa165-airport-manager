package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.ServiceCommunicator;
import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.flight.Flight;
import cz.muni.fi.pa165.api.flight.requests.FlightRequest;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.exception.FlightNotFoundException;
import cz.muni.fi.pa165.facade.FlightFacade;
import cz.muni.fi.pa165.exception.InvalidPlaneException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RestController
@RequiredArgsConstructor
@RequestMapping("/flights")
public class FlightController {

    @Autowired
    private final FlightFacade flightFacade;

    private final ServiceCommunicator serviceCommunicator = new ServiceCommunicator();

    @GetMapping
    public ResponseEntity<List<Flight>> getAllFlights(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        List<Flight> flights = flightFacade.getFlights(page, size);

        if (flights.isEmpty()) {
            return new ResponseEntity<>(flights, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(flights, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public Flight getFlight(@PathVariable UUID id) {
        return flightFacade.getFlight(id);
    }

    @PostMapping
    public CompletableFuture<Flight> createFlight(@RequestBody FlightRequest flightRequest) {
        return serviceCommunicator.validatePlan(flightRequest.getPlaneId()).thenCompose(isValid -> {
            if (isValid) {
                Flight flight = flightFacade.createFlight(flightRequest);
                return CompletableFuture.completedFuture(flight);
            }
            throw new InvalidPlaneException("Specified plane " + flightRequest.getPlaneId() + " is invalid!");
        });

       // Flight flight = flightFacade.createFlight(flightRequest);
       // return flight;
    }

    @PostMapping("/{id}")
    public Flight update(@PathVariable UUID id, @RequestBody FlightRequest flightRequest) {
        return flightFacade.updateFlight(id, flightRequest);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFlight(@PathVariable(value = "id") UUID id) {
        flightFacade.deleteFlight(id);
        return new ResponseEntity<>("Flight deleted sucessfully", HttpStatus.OK);
    }

    @GetMapping("/availablePilots")
    public Set<UUID> getAvailablePilots(@RequestParam PlaneType licenceType,
                                        @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date from,
                                        @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date to){
        return flightFacade.getAvailablePilots(licenceType, from, to);
    }

    @ExceptionHandler(FlightNotFoundException.class)
    public ResponseEntity<String> handleIssueReportNotFoundException(FlightNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Flight not found");
    }
}
