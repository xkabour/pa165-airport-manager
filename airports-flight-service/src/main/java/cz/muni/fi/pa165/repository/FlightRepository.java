package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.dao.Flight;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Repository
public interface FlightRepository extends JpaRepository<Flight, UUID> {
    Page<Flight> findAll(Pageable pageable);

    @Query(value = "SELECT DISTINCT Cast(steward_id AS varchar(50)) " +
            "FROM flightrealization fr " +
            "JOIN flight_pilot_ids fp ON fr.flight_id = fp.flight_id " +
            "WHERE departure_time > :from AND arrival_time < :to", nativeQuery = true)
    Set<UUID> findAvailable(@Param("from") Date from, @Param("to") Date to);
}
