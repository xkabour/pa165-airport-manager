package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.dao.IssueReport;
import cz.muni.fi.pa165.exception.IssueReportNotFoundException;
import cz.muni.fi.pa165.repository.IssueReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class IssueReportService {

    @Autowired
    private IssueReportRepository issueReportRepository;

    public List<IssueReport> getAllIssueReports() {
        return issueReportRepository.findAll();
    }

    public IssueReport get(UUID id) {
        return issueReportRepository.findById(id).orElseThrow(IssueReportNotFoundException::new);
    }

    public IssueReport createIssueReport(IssueReport issueReport) {
        return issueReportRepository.save(issueReport);
    }

    public IssueReport updateIssueReport(UUID id, IssueReport issueReport) {
        IssueReport existingIssueReport = issueReportRepository.findById(id).orElseThrow(IssueReportNotFoundException::new);
        existingIssueReport.setDescription(issueReport.getDescription());
        existingIssueReport.setFlightEncountered(issueReport.getFlightEncountered());

        IssueReport updateIssueReport = issueReportRepository.save(existingIssueReport);

        return updateIssueReport;
    }

    public void deleteIssueReport(UUID id) {
        issueReportRepository.deleteById(id);
    }
}
