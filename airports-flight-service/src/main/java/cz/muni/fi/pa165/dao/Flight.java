package cz.muni.fi.pa165.dao;

import cz.muni.fi.pa165.Constants;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;


@Data
@Entity
@Table(name = "flight")
@Inheritance(strategy = InheritanceType.JOINED)
public class Flight implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private UUID origin;
    private UUID destination;
    private UUID planeId;

    @ElementCollection
    @Column(name = "steward_id")
    private Set<UUID> pilotIds;

    @ElementCollection
    @Column(name = "steward_id")
    private Set<UUID> stewardIds;
}
