package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.flight.IssueReport;
import cz.muni.fi.pa165.api.flight.requests.IssueReportRequest;
import cz.muni.fi.pa165.service.IssueReportService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class IssueReportFacade {
    private final IssueReportService issueReportService;
    private final ModelMapper modelMapper = new ModelMapper();

    @PostConstruct
    public void init() {
        // Mapping IssueReportRequest to dao.IssueReport
        modelMapper.createTypeMap(IssueReportRequest.class, cz.muni.fi.pa165.dao.IssueReport.class)
                .addMappings(mapper -> {
                    mapper.map(IssueReportRequest::getFlightEncountered, cz.muni.fi.pa165.dao.IssueReport::setFlightEncountered);
                    mapper.map(IssueReportRequest::getDescription, cz.muni.fi.pa165.dao.IssueReport::setDescription);
                    mapper.map(IssueReportRequest::getPlaneId, cz.muni.fi.pa165.dao.IssueReport::setPlaneId);
                });

        // Mapping dao.IssueReport to api.IssueReport
        modelMapper.createTypeMap(cz.muni.fi.pa165.dao.IssueReport.class, IssueReport.class)
                .addMappings(mapper -> {
                    mapper.map(cz.muni.fi.pa165.dao.IssueReport::getId, IssueReport::setId);
                    mapper.map(cz.muni.fi.pa165.dao.IssueReport::getPlaneId, IssueReport::setPlaneId);
                    mapper.map(cz.muni.fi.pa165.dao.IssueReport::getFlightEncountered, IssueReport::setFlightEncountered);
                    mapper.map(cz.muni.fi.pa165.dao.IssueReport::getDescription, IssueReport::setDescription);
                });
    }

    public List<IssueReport> getAllIssueReports() {
        var result = issueReportService.getAllIssueReports();
        return result.stream().map(p -> modelMapper.map(p, IssueReport.class)).toList();
    }

    public IssueReport getIssueReport(UUID id) {
        cz.muni.fi.pa165.dao.IssueReport result = issueReportService.get(id);
        return modelMapper.map(result, IssueReport.class);
    }

    public IssueReport createIssueReport(IssueReportRequest issueReportRequest) {
        var issue = modelMapper.map(issueReportRequest, cz.muni.fi.pa165.dao.IssueReport.class);
        var result = issueReportService.createIssueReport(issue);
        return modelMapper.map(result, IssueReport.class);
    }


    public IssueReport updateIssueReport(UUID id, IssueReportRequest issueReportRequest) {
        var result = issueReportService.updateIssueReport(id, modelMapper.map(issueReportRequest, cz.muni.fi.pa165.dao.IssueReport.class));
        return modelMapper.map(result, IssueReport.class);
    }


    public void deleteIssueReport(UUID id) {
        issueReportService.deleteIssueReport(id);
    }
}
