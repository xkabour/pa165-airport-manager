package cz.muni.fi.pa165.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidPlaneException extends RuntimeException{
    public InvalidPlaneException() {
    }

    public InvalidPlaneException(String message) {
        super(message);
    }

    public InvalidPlaneException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPlaneException(Throwable cause) {
        super(cause);
    }

    public InvalidPlaneException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
