package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.api.employee.Employee;
import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.employee.client.HrClient;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.dao.Flight;
import cz.muni.fi.pa165.exception.FlightNotFoundException;
import cz.muni.fi.pa165.repository.FlightRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private HrClient hrClient;

    public List<Flight> getAllFlights(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Flight> flightPage = flightRepository.findAll(pageable);
        return flightPage.getContent().stream().collect(Collectors.toList());
    }

    public Flight getFlight(UUID id) {
        return flightRepository.findById(id).orElseThrow(FlightNotFoundException::new);
    }

    public Flight createFlight(Flight flight) {
        return flightRepository.save(flight);
    }

    public Flight updateFlight(UUID id, Flight flight) {
        Flight existingFlight = flightRepository.findById(id).orElseThrow(FlightNotFoundException::new);
        if (flight.getDestination() != null) {
            existingFlight.setDestination(flight.getDestination());
        }
        if (flight.getOrigin() != null) {
            existingFlight.setOrigin(flight.getOrigin());
        }
        if (flight.getPlaneId() != null) {
            existingFlight.setPlaneId(flight.getPlaneId());
        }
        if (!flight.getPilotIds().isEmpty()) {
            existingFlight.getPilotIds().addAll(flight.getPilotIds());
        }
        if (!flight.getStewardIds().isEmpty()) {
            existingFlight.getStewardIds().addAll(flight.getStewardIds());
        }

        Flight updateFlight = flightRepository.save(existingFlight);

        return updateFlight;
    }

    public void deleteFlight(UUID id) {
        flightRepository.deleteById(id);
    }

    public Set<UUID> getAvailablePilots(PlaneType licenceType, Date from, Date to) {
        try {
            List<Pilot> allPilots = hrClient.listLicencedPilots(licenceType);
            allPilots.stream()
                    .map(p -> p.getId())
                    .forEach(System.out::println);
            Set<UUID> allPilotId = allPilots.stream()
                    .map(Employee::getId)
                    .collect(Collectors.toSet());

            Set<UUID> availablePilots = flightRepository.findAvailable(from, to);
            System.out.println("aaa");
            availablePilots.stream()
                    .forEach(System.out::println);
            System.out.println("bb");
            return availablePilots;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e);
            return Set.of();
        }
    }
}
