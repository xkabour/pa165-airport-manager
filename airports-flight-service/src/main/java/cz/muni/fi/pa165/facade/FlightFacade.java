package cz.muni.fi.pa165.facade;


import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.flight.Flight;
import cz.muni.fi.pa165.api.flight.requests.FlightRequest;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.service.FlightService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FlightFacade {

    private final FlightService flightService;
    private final ModelMapper modelMapper = new ModelMapper();

    public List<Flight> getFlights(int page, int size) {
        var result = flightService.getAllFlights(page, size);
        return result.stream().map(p -> modelMapper.map(p, cz.muni.fi.pa165.api.flight.Flight.class)).toList();
    }

    public Flight getFlight(UUID id) {
        var result = flightService.getFlight(id);
        return modelMapper.map(result, Flight.class);
    }

    public Flight createFlight(FlightRequest flightRequest) {
        var result = flightService.createFlight(modelMapper.map(flightRequest, cz.muni.fi.pa165.dao.Flight.class));
        return modelMapper.map(result, Flight.class);
    }

    public Flight updateFlight(UUID id, FlightRequest flightRequest) {
        var result = flightService.updateFlight(id, modelMapper.map(flightRequest, cz.muni.fi.pa165.dao.Flight.class));

        return modelMapper.map(result, Flight.class);
    }


    public void deleteFlight(UUID id) {
        flightService.deleteFlight(id);
    }

    public Set<UUID> getAvailablePilots(PlaneType licenceType, Date from, Date to) {
        return flightService.getAvailablePilots(licenceType, from, to);
    }
}
