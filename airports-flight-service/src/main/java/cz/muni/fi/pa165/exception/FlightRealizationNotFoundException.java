package cz.muni.fi.pa165.exception;

public class FlightRealizationNotFoundException extends RuntimeException {

    public FlightRealizationNotFoundException() {
    }

    public FlightRealizationNotFoundException(String message) {
        super(message);
    }

    public FlightRealizationNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlightRealizationNotFoundException(Throwable cause) {
        super(cause);
    }

    public FlightRealizationNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
