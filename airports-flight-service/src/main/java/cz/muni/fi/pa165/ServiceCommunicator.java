package cz.muni.fi.pa165;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class ServiceCommunicator {

    public CompletableFuture<ServiceResponse> fetchServiceData(UUID id) {
//    public void fetchServiceData(UUID id) {
        ApiService apiService = RetrofitClient.getApiService();
        Call<ServiceResponse> call = apiService.getServiceData(id);
        CompletableFuture<ServiceResponse> future = new CompletableFuture<>();

        call.enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ServiceResponse serviceResponse = response.body();
                    System.out.println("Server Response");
                    System.out.println(serviceResponse);
                    future.complete(serviceResponse);
                    // Handle the successful response here
                    // System.out.println("Response: " + serviceResponse.toString());
                } else {
                    System.out.println("failed to fetch");
                    future.completeExceptionally(new RuntimeException("Request failed with code: " + response.code()));
                    // Handle the unsuccessful response here
                    // System.err.println("Request failed with code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                future.completeExceptionally(t);
                // Handle the failure here
                // t.printStackTrace();
            }
        });

         return future;
    }

    public CompletableFuture<Boolean> validatePlan(UUID planId) {

        return fetchServiceData(planId).thenApply(serviceResponse -> {
            System.out.println("response from service");
            System.out.println(serviceResponse);
            return serviceResponse != null;
        }).exceptionally(throwable -> {
            throwable.printStackTrace();
            return false;
        });
    }

}