package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.dao.FlightRealization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FlightRealizationRepository extends JpaRepository<FlightRealization, UUID> {
}
