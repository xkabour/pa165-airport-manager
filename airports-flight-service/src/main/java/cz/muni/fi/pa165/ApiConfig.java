package cz.muni.fi.pa165;

import cz.muni.fi.pa165.api.ClientUtils;
import cz.muni.fi.pa165.api.employee.client.HrClient;
import cz.muni.fi.pa165.api.employee.client.HrClientDescriptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiConfig {

    @Value("${airports.hr.service.url}")
    private String hrServiceUrl;

    @Bean
    public HrClient getHrClient(){
        System.out.println(hrServiceUrl);
        return new HrClient(ClientUtils.configuredRetrofitBuilder(hrServiceUrl)
            .build()
            .create(HrClientDescriptor.class));
    }
}

