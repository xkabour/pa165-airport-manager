package cz.muni.fi.pa165;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.UUID;

public interface ApiService {

    @GET("/plane/{id}")
    Call<ServiceResponse> getServiceData(@Path("id") UUID id);
}
