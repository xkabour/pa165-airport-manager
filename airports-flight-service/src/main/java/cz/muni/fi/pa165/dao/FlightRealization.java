package cz.muni.fi.pa165.dao;

import cz.muni.fi.pa165.Constants;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "flightrealization")
@Inheritance(strategy = InheritanceType.JOINED)
public class FlightRealization implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private UUID flightId;
    private String report;

    private Duration duration;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;

    private double kilometersFlown;
}
