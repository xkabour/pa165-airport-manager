package cz.muni.fi.pa165.facade;


import cz.muni.fi.pa165.api.flight.requests.IssueReportRequest;
import cz.muni.fi.pa165.dao.IssueReport;
import cz.muni.fi.pa165.service.IssueReportService;
import cz.muni.fi.pa165.utils.TestApiFactory;
import cz.muni.fi.pa165.utils.TestDaoFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class IssueReportFacadeTest {


    @Mock
    private IssueReportService issueReportService;

    @InjectMocks
    private IssueReportFacade issueReportFacade;

    @BeforeEach
    public void simulate_post_construct() {
        issueReportFacade.init();
    }

    @Test
    public void createIssueReport() {
        cz.muni.fi.pa165.api.flight.IssueReport issueReport = TestApiFactory.getIssueReport();
        IssueReport daoIssueReport = TestDaoFactory.getIssueReport();
        IssueReportRequest issueReportRequest = TestApiFactory.getIssueReportRequest();

        // Use argument matchers to handle dynamic UUID
        Mockito.when(issueReportService.createIssueReport(Mockito.any(IssueReport.class)))
                .thenReturn(daoIssueReport);

        cz.muni.fi.pa165.api.flight.IssueReport returnedIssueReport = issueReportFacade.createIssueReport(issueReportRequest);

        assertEquals(issueReport, returnedIssueReport);
        Mockito.verify(issueReportService, Mockito.times(1)).createIssueReport(Mockito.any(IssueReport.class));
    }

    @Test
    public void getIssueReport() {
        UUID id = UUID.randomUUID();
        cz.muni.fi.pa165.dao.IssueReport daoIssueReport = TestDaoFactory.getIssueReport();
        cz.muni.fi.pa165.api.flight.IssueReport apiIssueReport = TestApiFactory.getIssueReport();

        Mockito.when(issueReportService.get(id)).thenReturn(daoIssueReport);

        cz.muni.fi.pa165.api.flight.IssueReport returnedIssueReport = issueReportFacade.getIssueReport(id);

        assertEquals(apiIssueReport, returnedIssueReport);
        Mockito.verify(issueReportService, Mockito.times(1)).get(id);
    }

    @Test
    public void updateIssueReport() {
        UUID id = UUID.randomUUID();
        cz.muni.fi.pa165.dao.IssueReport daoIssueReport = TestDaoFactory.getIssueReport();
        cz.muni.fi.pa165.api.flight.IssueReport apiIssueReport = TestApiFactory.getIssueReport();
        IssueReportRequest issueReportRequest = TestApiFactory.getIssueReportRequest();

        Mockito.when(issueReportService.updateIssueReport(Mockito.eq(id), Mockito.any(IssueReport.class)))
                .thenReturn(daoIssueReport);

        cz.muni.fi.pa165.api.flight.IssueReport returnedIssueReport = issueReportFacade.updateIssueReport(id, issueReportRequest);

        assertEquals(apiIssueReport, returnedIssueReport);
        Mockito.verify(issueReportService, Mockito.times(1))
                .updateIssueReport(Mockito.eq(id), Mockito.any(IssueReport.class));
    }

    @Test
    public void deleteIssueReport() {
        UUID id = UUID.randomUUID();

        Mockito.doNothing().when(issueReportService).deleteIssueReport(id);

        issueReportFacade.deleteIssueReport(id);

        Mockito.verify(issueReportService, Mockito.times(1)).deleteIssueReport(id);
    }
}
