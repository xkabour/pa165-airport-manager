package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.flight.Flight;
import cz.muni.fi.pa165.api.flight.requests.FlightRequest;
import cz.muni.fi.pa165.exception.FlightNotFoundException;
import cz.muni.fi.pa165.facade.FlightFacade;
import cz.muni.fi.pa165.utils.ObjectConverter;
import cz.muni.fi.pa165.utils.TestApiFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = FlightController.class)
public class FlightControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlightFacade flightFacade;

    @Test
    void getAllFlights_noFlightsFound_returnsNoContent() throws Exception {
        Mockito.when(flightFacade.getFlights(0, 10)).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/flights")
                        .param("page", "0")
                        .param("size", "10")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void getAllFlights_flightsFound_returnsFlights() throws Exception {
        Flight flight = TestApiFactory.getFlightEntity();
        Mockito.when(flightFacade.getFlights(0, 10)).thenReturn(Collections.singletonList(flight));

        String responseJson = mockMvc.perform(get("/flights")
                        .param("page", "0")
                        .param("size", "10")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Flight[] response = ObjectConverter.convertJsonToObject(responseJson, Flight[].class);

        assertThat(response).containsExactly(flight);
    }

    @Test
    void getFlight_flightFound_returnsFlight() throws Exception {
        UUID id = UUID.randomUUID();
        Flight flight = TestApiFactory.getFlightEntity();
        Mockito.when(flightFacade.getFlight(id)).thenReturn(flight);

        String responseJson = mockMvc.perform(get("/flights/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Flight response = ObjectConverter.convertJsonToObject(responseJson, Flight.class);

        assertThat(response).isEqualTo(flight);
    }

    @Test
    void getFlight_flightNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(flightFacade.getFlight(id)).thenThrow(new FlightNotFoundException());

        mockMvc.perform(get("/flights/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        Mockito.verify(flightFacade, Mockito.times(1)).getFlight(id);
    }

    @Test
    void updateFlight_flightUpdated_returnsFlight() throws Exception {
        UUID id = UUID.randomUUID();
        FlightRequest flightRequest = TestApiFactory.getFlightRequest();
        Flight flight = TestApiFactory.getFlightEntity();
        Mockito.when(flightFacade.updateFlight(Mockito.eq(id), Mockito.any(FlightRequest.class))).thenReturn(flight);

        String requestJson = ObjectConverter.convertObjectToJson(flightRequest);
        String responseJson = mockMvc.perform(post("/flights/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Flight response = ObjectConverter.convertJsonToObject(responseJson, Flight.class);

        assertThat(response).isEqualTo(flight);
    }

    @Test
    void updateFlight_flightNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        FlightRequest flightRequest = TestApiFactory.getFlightRequest();
        Mockito.when(flightFacade.updateFlight(Mockito.eq(id), Mockito.any(FlightRequest.class))).thenThrow(new FlightNotFoundException());

        String requestJson = ObjectConverter.convertObjectToJson(flightRequest);
        mockMvc.perform(post("/flights/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isNotFound());

        Mockito.verify(flightFacade, Mockito.times(1)).updateFlight(Mockito.eq(id), Mockito.any(FlightRequest.class));
    }

    @Test
    void deleteFlight_flightDeleted_returnsOk() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(flightFacade).deleteFlight(id);

        mockMvc.perform(delete("/flights/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Mockito.verify(flightFacade, Mockito.times(1)).deleteFlight(id);
    }
}
