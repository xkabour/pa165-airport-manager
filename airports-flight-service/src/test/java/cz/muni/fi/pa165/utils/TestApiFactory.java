package cz.muni.fi.pa165.utils;

import cz.muni.fi.pa165.api.flight.Flight;
import cz.muni.fi.pa165.api.flight.FlightRealization;
import cz.muni.fi.pa165.api.flight.IssueReport;
import cz.muni.fi.pa165.api.flight.requests.FlightRealizationRequest;
import cz.muni.fi.pa165.api.flight.requests.FlightRequest;
import cz.muni.fi.pa165.api.flight.requests.IssueReportRequest;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Component
public class TestApiFactory {


    public static Flight getFlightEntity() {
        UUID id = new UUID(0x1, 0xf);
        Flight flight = new Flight();
        flight.setPlaneId(new UUID(0x2, 0xf));
        flight.setDestination(new UUID(0x4, 0xf));
        flight.setOrigin(new UUID(0x3, 0xf));
        flight.setId(id);
        Set<UUID> pilots = new HashSet<>();
        pilots.add(new UUID(0x5, 0xf));
        pilots.add(new UUID(0x6, 0xf));
        flight.setPilotIds(pilots);

        Set<UUID> stewards = new HashSet<>();
        stewards.add(new UUID(0x7, 0xf));
        stewards.add(new UUID(0x8, 0xf));
        flight.setStewardIds(stewards);

        return flight;
    }


    public static FlightRealization getFlightRealizationEntity() {
        UUID id = new UUID(0x1, 0xf);
        FlightRealization flightRealization = new FlightRealization();
        flightRealization.setDepartureTime(LocalDateTime.of(2002, 2, 2, 2, 2, 2));
        flightRealization.setArrivalTime(LocalDateTime.of(2002, 2, 2, 2, 2, 2));
        flightRealization.setFlightId(new UUID(0x2, 0xf));
        flightRealization.setReport("report");
        flightRealization.setDuration(Duration.ofHours(1));
        flightRealization.setKilometersFlown(850.0);
        flightRealization.setId(id);

        return flightRealization;
    }

    public static IssueReport getIssueReport() {
        IssueReport issueReport = new IssueReport();
        issueReport.setId(new UUID(0x1, 0xf));
        issueReport.setPlaneId(new UUID(0x2, 0xf));
        issueReport.setFlightEncountered(new UUID(0x3, 0xf));
        issueReport.setDescription("description");

        return issueReport;
    }

    public static FlightRequest getFlightRequest() {
        Set<UUID> pilots = new HashSet<>();
        pilots.add(UUID.randomUUID());
        pilots.add(UUID.randomUUID());

        Set<UUID> stewards = new HashSet<>();
        stewards.add(UUID.randomUUID());
        stewards.add(UUID.randomUUID());
        FlightRequest flightRequest = new FlightRequest(
                new UUID(0x1, 0xf),
                new UUID(0x1, 0xf),
                new UUID(0x1, 0xf),
                pilots, stewards
        );
        return flightRequest;
    }

    public static FlightRealizationRequest getFlightRelizationRequest() {
        UUID flightId = new UUID(0x1, 0xf);

        FlightRealizationRequest flightRealizationRequest = new FlightRealizationRequest(
                flightId,
                "report",
                Duration.ofHours(1),
                LocalDateTime.now(),
                LocalDateTime.now(),
                2.5
        );

        return flightRealizationRequest;
    }

    public static IssueReportRequest getIssueReportRequest() {
        IssueReportRequest issueReportRequest = new IssueReportRequest(
                new UUID(0x2, 0xf),
                new UUID(0x3, 0xf),
                "description"
        );
        return issueReportRequest;
    }
}
