package cz.muni.fi.pa165.utils;

import cz.muni.fi.pa165.dao.Flight;
import cz.muni.fi.pa165.dao.FlightRealization;
import cz.muni.fi.pa165.dao.IssueReport;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Component
public class TestDaoFactory {

    public static Flight getFlightEntity() {
        Flight flight = new Flight();
        flight.setId(new UUID(0x1, 0xf));
        flight.setPlaneId(new UUID(0x2, 0xf));
        flight.setOrigin(new UUID(0x3, 0xf));
        flight.setDestination(new UUID(0x4, 0xf));

        Set<UUID> pilots = new HashSet<>();
        pilots.add(new UUID(0x5, 0xf));
        pilots.add(new UUID(0x6, 0xf));
        flight.setPilotIds(pilots);

        Set<UUID> stewards = new HashSet<>();
        stewards.add(new UUID(0x7, 0xf));
        stewards.add(new UUID(0x8, 0xf));
        flight.setStewardIds(stewards);

        return flight;
    }


    public static FlightRealization getflightRealizationEntity() {
        UUID id = new UUID(0x1, 0xf);
        FlightRealization flightRealization = new FlightRealization();
        flightRealization.setDepartureTime(LocalDateTime.of(2002, 2, 2, 2, 2, 2));
        flightRealization.setArrivalTime(LocalDateTime.of(2002, 2, 2, 2, 2, 2));
        flightRealization.setFlightId(new UUID(0x2, 0xf));
        flightRealization.setReport("report");
        flightRealization.setDuration(Duration.ofHours(1));
        flightRealization.setKilometersFlown(850.0);
        flightRealization.setId(id);

        return flightRealization;
    }

    public static IssueReport getIssueReport() {
        IssueReport issueReport = new IssueReport();
        issueReport.setId(new UUID(0x1, 0xf));
        issueReport.setPlaneId(new UUID(0x2, 0xf));
        issueReport.setFlightEncountered(new UUID(0x3, 0xf));
        issueReport.setDescription("description");

        return issueReport;
    }
}
