package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.dao.Flight;
import cz.muni.fi.pa165.exception.FlightNotFoundException;
import cz.muni.fi.pa165.repository.FlightRepository;
import cz.muni.fi.pa165.utils.TestDaoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {

    @Mock
    private FlightRepository flightRepository;
    @InjectMocks
    private FlightService flightService;

    @Test
    public void getFlight_flightFound_returnsFlight() {
        // Prepare a sample flight ID
        UUID id = UUID.randomUUID();

        // Create a sample flight object
        Flight flight = TestDaoFactory.getFlightEntity();

        // Mocking the behavior of the flightRepository.findById() method
        Mockito.when(flightRepository.findById(id)).thenReturn(Optional.of(flight));

        // Calling the method to be tested
        Flight foundFlight = flightService.getFlight(id);

        // Assertions
        Assertions.assertEquals(foundFlight, flight); // Check if the returned flight matches the one we created
    }

    @Test
    void getById_flightNotFound_throwsResourceNotFoundException() {
        // Prepare a random non-existent flight ID
        UUID id = UUID.randomUUID();

        // Mock the behavior of the flightRepository.findById() method to return an empty Optional
        Mockito.when(flightRepository.findById(id)).thenReturn(Optional.empty());

        // Assert that calling flightService.getFlight(id) should throw FlightNotFoundException
        assertThrows(FlightNotFoundException.class, () -> flightService.getFlight(id));
    }


    @Test
    void createFlight() {
        // Create a sample flight object
        Flight flight = TestDaoFactory.getFlightEntity();

        // Mocking the behavior of the flightRepository.save() method
        Mockito.when(flightRepository.save(flight)).thenReturn(flight);

        // Calling the method to be tested
        Flight createdFlight = flightService.createFlight(flight);

        // Assertions
        assertEquals(flight, createdFlight); // Check if returned flight matches the one we created
        Mockito.verify(flightRepository, Mockito.times(1)).save(flight); // Verify that save method was called exactly once with the correct parameter
    }

    @Test
    void updateFlight_flightNotFound_throwsFlightNotFoundException() {
        UUID id = UUID.randomUUID(); // Generate a random flight ID
        Flight update = new Flight(); // Prepare an update object

        // Assert that calling flightService.updateFlight(id, update) should throw FlightNotFoundException
        assertThrows(FlightNotFoundException.class, () -> flightService.updateFlight(id, update));

        // Verify that stewardRepository.save() was not called
        Mockito.verify(flightRepository, Mockito.times(0)).save(Mockito.any());
    }


    @Test
    void updateFlight_flightFound() {
        UUID id = new UUID(0x1, 0xf);
        var destId = new UUID(0x2, 0xf);

        Flight existingFlight = TestDaoFactory.getFlightEntity();

        Flight update = TestDaoFactory.getFlightEntity();

        Flight updatedFlight = TestDaoFactory.getFlightEntity();


        Mockito.when(flightRepository.findById(id)).thenReturn(Optional.of(existingFlight));
        Mockito.when(flightRepository.save(Mockito.any(Flight.class))).thenAnswer(invocation -> invocation.getArgument(0));


        assertEquals(updatedFlight, flightService.updateFlight(id, update));
        Mockito.verify(flightRepository, Mockito.times(1)).save(updatedFlight);
    }

}
