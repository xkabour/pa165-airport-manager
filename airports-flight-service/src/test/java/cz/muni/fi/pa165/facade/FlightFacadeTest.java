package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.flight.Flight;
import cz.muni.fi.pa165.api.flight.requests.FlightRequest;
import cz.muni.fi.pa165.exception.FlightNotFoundException;
import cz.muni.fi.pa165.service.FlightService;
import cz.muni.fi.pa165.utils.TestApiFactory;
import cz.muni.fi.pa165.utils.TestDaoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FlightFacadeTest {

    @Mock
    private FlightService flightService;

    @InjectMocks
    private FlightFacade flightFacade;

    @Test
    public void getFlights() {
        List<cz.muni.fi.pa165.dao.Flight> daoFlights = List.of(TestDaoFactory.getFlightEntity());
        List<Flight> apiFlights = List.of(TestApiFactory.getFlightEntity());

        when(flightService.getAllFlights(anyInt(), anyInt())).thenReturn(daoFlights);

        List<Flight> result = flightFacade.getFlights(0, 10);

        assertEquals(apiFlights, result);
        verify(flightService, times(1)).getAllFlights(0, 10);
    }

    @Test
    public void getFlight_flightFound() {
        UUID id = UUID.randomUUID();
        cz.muni.fi.pa165.dao.Flight daoFlight = TestDaoFactory.getFlightEntity();
        Flight apiFlight = TestApiFactory.getFlightEntity();

        when(flightService.getFlight(id)).thenReturn(daoFlight);

        Flight result = flightFacade.getFlight(id);
        assertNotNull(result);
        assertEquals(apiFlight, result);

        verify(flightService, times(1)).getFlight(id);
    }

    @Test
    public void getFlight_flightNotFound_throwsFlightNotFoundException() {
        UUID id = UUID.randomUUID();
        when(flightService.getFlight(id)).thenThrow(new FlightNotFoundException());

        assertThrows(FlightNotFoundException.class, () -> flightFacade.getFlight(id));

        verify(flightService, times(1)).getFlight(id);
    }

    @Test
    public void createFlight() {
        FlightRequest flightRequest = TestApiFactory.getFlightRequest();
        cz.muni.fi.pa165.dao.Flight daoFlight = TestDaoFactory.getFlightEntity();
        Flight apiFlight = TestApiFactory.getFlightEntity();

        when(flightService.createFlight(any(cz.muni.fi.pa165.dao.Flight.class))).thenReturn(daoFlight);

        Flight result = flightFacade.createFlight(flightRequest);
        assertEquals(apiFlight, result);

        verify(flightService, times(1)).createFlight(any(cz.muni.fi.pa165.dao.Flight.class));
    }

    @Test
    public void updateFlight() {
        UUID id = UUID.randomUUID();
        FlightRequest flightRequest = TestApiFactory.getFlightRequest();
        cz.muni.fi.pa165.dao.Flight daoFlight = TestDaoFactory.getFlightEntity();
        Flight apiFlight = TestApiFactory.getFlightEntity();

        when(flightService.updateFlight(eq(id), any(cz.muni.fi.pa165.dao.Flight.class))).thenReturn(daoFlight);

        Flight result = flightFacade.updateFlight(id, flightRequest);
        assertEquals(apiFlight, result);

        verify(flightService, times(1)).updateFlight(eq(id), any(cz.muni.fi.pa165.dao.Flight.class));
    }

    @Test
    public void deleteFlight() {
        UUID id = UUID.randomUUID();
        doNothing().when(flightService).deleteFlight(id);

        flightFacade.deleteFlight(id);

        verify(flightService, times(1)).deleteFlight(id);
    }

}
