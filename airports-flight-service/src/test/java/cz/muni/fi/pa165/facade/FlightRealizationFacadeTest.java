package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.api.flight.requests.FlightRealizationRequest;
import cz.muni.fi.pa165.dao.FlightRealization;
import cz.muni.fi.pa165.exception.FlightRealizationNotFoundException;
import cz.muni.fi.pa165.service.FlightRealizationService;
import cz.muni.fi.pa165.service.FlightService;
import cz.muni.fi.pa165.utils.TestApiFactory;
import cz.muni.fi.pa165.utils.TestDaoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FlightRealizationFacadeTest {

    @Mock
    private FlightRealizationService flightRealizationService;

    @InjectMocks
    private FlightRealizationFacade flightRealizationFacade;

    @Test
    public void getAllFlightRealizations() {
        List<FlightRealization> daoFlightRealizations = List.of(TestDaoFactory.getflightRealizationEntity());
        List<cz.muni.fi.pa165.api.flight.FlightRealization> apiFlightRealizations = List.of(TestApiFactory.getFlightRealizationEntity());

        when(flightRealizationService.getAllFlightRealizations()).thenReturn(daoFlightRealizations);

        List<cz.muni.fi.pa165.api.flight.FlightRealization> result = flightRealizationFacade.getAllFlightRealizations();

        assertEquals(apiFlightRealizations, result);
        verify(flightRealizationService, times(1)).getAllFlightRealizations();
    }

    @Test
    public void getFlightRealization_flightFound() {
        UUID id = new UUID(0x1, 0xf);
        FlightRealization daoFlightRealization = TestDaoFactory.getflightRealizationEntity();
        cz.muni.fi.pa165.api.flight.FlightRealization apiFlightRealization = TestApiFactory.getFlightRealizationEntity();

        when(flightRealizationService.getFlightRealizationById(id)).thenReturn(daoFlightRealization);

        cz.muni.fi.pa165.api.flight.FlightRealization result = flightRealizationFacade.getFlightRealization(id);
        assertNotNull(result);
        assertEquals(apiFlightRealization, result);

        verify(flightRealizationService, times(1)).getFlightRealizationById(id);
    }

    @Test
    public void getFlightRealization_flightNotFound_throwsFlightRealizationNotFoundException() {
        UUID id = UUID.randomUUID();
        when(flightRealizationService.getFlightRealizationById(id)).thenThrow(new FlightRealizationNotFoundException());

        assertThrows(FlightRealizationNotFoundException.class, () -> flightRealizationFacade.getFlightRealization(id));

        verify(flightRealizationService, times(1)).getFlightRealizationById(id);
    }

    @Test
    public void createFlightRealization() {
        FlightRealizationRequest flightRealizationRequest = TestApiFactory.getFlightRelizationRequest();
        FlightRealization daoFlightRealization = TestDaoFactory.getflightRealizationEntity();
        cz.muni.fi.pa165.api.flight.FlightRealization apiFlightRealization = TestApiFactory.getFlightRealizationEntity();

        when(flightRealizationService.createFlightRealization(any(cz.muni.fi.pa165.dao.FlightRealization.class))).thenReturn(daoFlightRealization);

        cz.muni.fi.pa165.api.flight.FlightRealization result = flightRealizationFacade.createFlightRealization(flightRealizationRequest);
        assertEquals(apiFlightRealization, result);

        verify(flightRealizationService, times(1)).createFlightRealization(any(cz.muni.fi.pa165.dao.FlightRealization.class));
    }

    @Test
    public void updateFlightRealization() {
        UUID id = new UUID(0x1, 0xf);
        FlightRealizationRequest flightRealizationRequest = TestApiFactory.getFlightRelizationRequest();
        FlightRealization daoFlightRealization = TestDaoFactory.getflightRealizationEntity();
        cz.muni.fi.pa165.api.flight.FlightRealization apiFlightRealization = TestApiFactory.getFlightRealizationEntity();

        when(flightRealizationService.updateFlightRealization(eq(id), any(cz.muni.fi.pa165.dao.FlightRealization.class))).thenReturn(daoFlightRealization);

        cz.muni.fi.pa165.api.flight.FlightRealization result = flightRealizationFacade.updateFlightRealization(id, flightRealizationRequest);
        assertEquals(apiFlightRealization, result);

        verify(flightRealizationService, times(1)).updateFlightRealization(eq(id), any(cz.muni.fi.pa165.dao.FlightRealization.class));
    }

    @Test
    public void deleteFlightRealization() {
        UUID id = new UUID(0x1, 0xf);
        doNothing().when(flightRealizationService).deleteFlightRealization(id);

        flightRealizationFacade.deleteFlightRealization(id);

        verify(flightRealizationService, times(1)).deleteFlightRealization(id);
    }
}
