package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.flight.IssueReport;
import cz.muni.fi.pa165.api.flight.requests.IssueReportRequest;
import cz.muni.fi.pa165.exception.IssueReportNotFoundException;
import cz.muni.fi.pa165.facade.IssueReportFacade;
import cz.muni.fi.pa165.utils.ObjectConverter;
import cz.muni.fi.pa165.utils.TestApiFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = {IssueReportController.class})
public class IssueReportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IssueReportFacade issueReportFacade;

    @Test
    void findById_issueReportFound_returnsIssueReport() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        Mockito.when(issueReportFacade.getIssueReport(id)).thenReturn(TestApiFactory.getIssueReport());

        String responseJson = mockMvc.perform(get("/issuereports/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        IssueReport response = ObjectConverter.convertJsonToObject(responseJson, IssueReport.class);
        assertThat(responseJson).isEqualTo("{\"id\":\"00000000-0000-0001-0000-00000000000f\",\"planeId\":\"00000000-0000-0002-0000-00000000000f\",\"flightEncountered\":\"00000000-0000-0003-0000-00000000000f\",\"description\":\"description\"}");

        assertThat(response).isEqualTo(TestApiFactory.getIssueReport());
    }


    @Test
    void findById_issueReportNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(issueReportFacade.getIssueReport(id)).thenThrow(new IssueReportNotFoundException());

        mockMvc.perform(get("/issuereports/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        Mockito.verify(issueReportFacade, Mockito.times(1)).getIssueReport(id);
    }

    @Test
    void updateIssueReport_IssueReportFound_returnsIssueReport() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        IssueReport updatedIssueReport = TestApiFactory.getIssueReport();
        Mockito.when(issueReportFacade.updateIssueReport(Mockito.eq(id), Mockito.any(IssueReportRequest.class))).thenReturn(updatedIssueReport);

        String requestJson =
                "{" +
                        "\"id\":\"" + id + "\"," +
                        "\"planeId\":\"" + id + "\"," +
                        "\"flightEncountered\":\"" + id + "\"," +
                        "\"description\":\"description\"" +
                        "}";
        String responseJson = mockMvc.perform(put("/issuereports/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                )
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        IssueReport response = ObjectConverter.convertJsonToObject(responseJson, IssueReport.class);
        assertThat(response).isEqualTo(updatedIssueReport);
        Mockito.verify(issueReportFacade, Mockito.times(1)).updateIssueReport(Mockito.eq(id), Mockito.any(IssueReportRequest.class));
    }


    @Test
    public void updateIssueReport_IssueReportNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();

        String requestJson =
                "{" +
                        "\"id\":\"" + id + "\"," +
                        "\"planeId\":\"" + id + "\"," +
                        "\"flightEncountered\":\"" + id + "\"," +
                        "\"description\":\"description\"" +
                        "}";

        Mockito.when(issueReportFacade.updateIssueReport(Mockito.eq(id), Mockito.any(IssueReportRequest.class))).thenThrow(new IssueReportNotFoundException());

        mockMvc.perform(put("/issuereports/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isNotFound());

        Mockito.verify(issueReportFacade, Mockito.times(1)).updateIssueReport(Mockito.eq(id), Mockito.any(IssueReportRequest.class));
    }


    @Test
    public void deleteIssueReport_IssueReportFound() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(issueReportFacade).deleteIssueReport(id);
        RequestBuilder request = MockMvcRequestBuilders.delete("/issuereports/{id}", id).accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request).andExpect(status().isOk());
        Mockito.verify(issueReportFacade, Mockito.times(1)).deleteIssueReport(id);
    }
}
