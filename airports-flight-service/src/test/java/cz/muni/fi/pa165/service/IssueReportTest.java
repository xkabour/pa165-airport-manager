package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.dao.IssueReport;
import cz.muni.fi.pa165.exception.IssueReportNotFoundException;
import cz.muni.fi.pa165.repository.IssueReportRepository;
import cz.muni.fi.pa165.utils.TestDaoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class IssueReportTest {

    @Mock
    private IssueReportRepository issueReportRepository;

    @InjectMocks
    private IssueReportService issueReportService;


    @Test
    public void getIssueReport_issueReportFound_returnsIssueReport() {
        UUID id = UUID.randomUUID();
        IssueReport issueReport = TestDaoFactory.getIssueReport();
        Mockito.when(issueReportRepository.findById(id)).thenReturn(Optional.of(issueReport));
        IssueReport foundIssueReport = issueReportService.get(id);
        Assertions.assertEquals(foundIssueReport, issueReport);
    }

    @Test
    void getById_issueReportNotFound_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();
        Mockito.when(issueReportRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(IssueReportNotFoundException.class, () -> issueReportService.get(id));
    }


    @Test
    void createIssueReport() {
        IssueReport issueReport = TestDaoFactory.getIssueReport();
        Mockito.when(issueReportRepository.save(issueReport)).thenReturn(issueReport);
        IssueReport createdIssueReport = issueReportService.createIssueReport(issueReport);
        assertEquals(issueReport, createdIssueReport);
        Mockito.verify(issueReportRepository, Mockito.times(1)).save(issueReport);
    }

    @Test
    void updateIssueReport_issueReportNotFound_throwsIssueReportNotFoundException() {
        UUID id = UUID.randomUUID();
        IssueReport update = new IssueReport();
        assertThrows(IssueReportNotFoundException.class, () -> issueReportService.updateIssueReport(id, update));
        Mockito.verify(issueReportRepository, Mockito.times(0)).save(Mockito.any());
    }


    @Test
    void updateIssueReport_issueReportFound() {
        UUID id = new UUID(0x1, 0xf);
        UUID planeId = new UUID(0x2, 0xf);
        UUID flightId = new UUID(0x3, 0xf);
        IssueReport issueReport = TestDaoFactory.getIssueReport();

        IssueReport update = new IssueReport();
        update.setDescription("updateDescription");
        update.setPlaneId(planeId);
        update.setId(id);
        update.setFlightEncountered(flightId);

        IssueReport updatedIssueReport = TestDaoFactory.getIssueReport();
        updatedIssueReport.setId(id);
        updatedIssueReport.setDescription("updateDescription");
        updatedIssueReport.setPlaneId(planeId);
        update.setFlightEncountered(flightId);

        Mockito.when(issueReportRepository.findById(id)).thenReturn(Optional.of(issueReport));
        Mockito.when(issueReportRepository.save(Mockito.any(IssueReport.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertEquals(updatedIssueReport, issueReportService.updateIssueReport(id, update));
        Mockito.verify(issueReportRepository, Mockito.times(1)).save(updatedIssueReport);
    }
}
