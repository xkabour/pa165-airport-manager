package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.dao.FlightRealization;
import cz.muni.fi.pa165.exception.FlightRealizationNotFoundException;
import cz.muni.fi.pa165.repository.FlightRealizationRepository;
import cz.muni.fi.pa165.utils.TestDaoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class FlightRealizationServiceTest {

    @Mock
    private FlightRealizationRepository flightRealizationRepository;
    @InjectMocks
    private FlightRealizationService flightRealizationService;

    @Test
    public void getFlightRealization_flightRealizationFound_returnsFlight() {
        // Prepare a sample flight ID
        UUID id = UUID.randomUUID();

        // Create a sample flight object
        FlightRealization flight = TestDaoFactory.getflightRealizationEntity();

        // Mocking the behavior of the flightRepository.findById() method
        Mockito.when(flightRealizationRepository.findById(id)).thenReturn(Optional.of(flight));

        // Calling the method to be tested
        FlightRealization foundFlight = flightRealizationService.getFlightRealizationById(id);

        // Assertions
        Assertions.assertEquals(foundFlight, flight); // Check if the returned flight matches the one we created
    }

    @Test
    void getById_flightRealizationNotFound_throwsResourceNotFoundException() {
        // Prepare a random non-existent flight ID
        UUID id = UUID.randomUUID();

        // Mock the behavior of the flightRepository.findById() method to return an empty Optional
        Mockito.when(flightRealizationRepository.findById(id)).thenReturn(Optional.empty());

        // Assert that calling flightService.getFlight(id) should throw FlightNotFoundException
        assertThrows(FlightRealizationNotFoundException.class, () -> flightRealizationService.getFlightRealizationById(id));
    }


    @Test
    void createFlightRealization() {
        // Create a sample flight object
        FlightRealization flight = TestDaoFactory.getflightRealizationEntity();

        // Mocking the behavior of the flightRepository.save() method
        Mockito.when(flightRealizationRepository.save(flight)).thenReturn(flight);

        // Calling the method to be tested
        FlightRealization createdFlight = flightRealizationService.createFlightRealization(flight);

        // Assertions
        assertEquals(flight, createdFlight); // Check if returned flight matches the one we created
        Mockito.verify(flightRealizationRepository, Mockito.times(1)).save(flight); // Verify that save method was called exactly once with the correct parameter
    }

    @Test
    void updateFlightRealization_flightRealizationNotFound_throwsFlightRealizationNotFoundException() {
        UUID id = UUID.randomUUID(); // Generate a random flight ID
        FlightRealization update = new FlightRealization(); // Prepare an update object

        // Assert that calling flightService.updateFlight(id, update) should throw FlightNotFoundException
        assertThrows(FlightRealizationNotFoundException.class, () -> flightRealizationService.updateFlightRealization(id, update));

        // Verify that stewardRepository.save() was not called
        Mockito.verify(flightRealizationRepository, Mockito.times(0)).save(Mockito.any());
    }


    @Test
    void updateFlightRealization_flighrealizationtFound() {
        UUID id = new UUID(0x1, 0xf);

        FlightRealization existingFlightRealization = TestDaoFactory.getflightRealizationEntity();

        FlightRealization update = TestDaoFactory.getflightRealizationEntity();
        update.setDuration(Duration.ofHours(2));
        update.setFlightId(id);

        FlightRealization updatedFlightRealization = TestDaoFactory.getflightRealizationEntity();
        updatedFlightRealization.setDuration(Duration.ofHours(2));

        Mockito.when(flightRealizationRepository.findById(id)).thenReturn(Optional.of(existingFlightRealization));
        Mockito.when(flightRealizationRepository.save(Mockito.any(FlightRealization.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertEquals(updatedFlightRealization, flightRealizationService.updateFlightRealization(id, update));
        Mockito.verify(flightRealizationRepository, Mockito.times(1)).save(updatedFlightRealization);
    }
}
