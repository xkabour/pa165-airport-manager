package cz.muni.fi.pa165.rest;

import cz.muni.fi.pa165.api.flight.FlightRealization;
import cz.muni.fi.pa165.api.flight.requests.FlightRealizationRequest;
import cz.muni.fi.pa165.exception.FlightRealizationNotFoundException;
import cz.muni.fi.pa165.facade.FlightRealizationFacade;
import cz.muni.fi.pa165.utils.ObjectConverter;
import cz.muni.fi.pa165.utils.TestApiFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = {FlightRealizationController.class})
public class FlightRealizationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlightRealizationFacade flightRealizationFacade;

    @Test
    void findById_flightRealizationFound_returnsFlightRealization() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        Mockito.when(flightRealizationFacade.getFlightRealization(id)).thenReturn(TestApiFactory.getFlightRealizationEntity());

        String responseJson = mockMvc.perform(get("/flightrealization/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        FlightRealization response = ObjectConverter.convertJsonToObject(responseJson, FlightRealization.class);

        assertThat(responseJson).isEqualTo("{" +
                "\"id\":\"" + id + "\"," +
                "\"flightId\":\"" + new UUID(0x2, 0xf) + "\"," +
                "\"report\":\"report\"," +
                "\"duration\":\"" + Duration.ofHours(1) + "\"," +
                "\"departureTime\":\"" + LocalDateTime.of(2002, 2, 2, 2, 2, 2) + "\"," +
                "\"arrivalTime\":\"" + LocalDateTime.of(2002, 2, 2, 2, 2, 2) + "\"," +
                "\"kilometersFlown\":" + 850.0 +
                "}"
        );


        assertThat(response).isEqualTo(TestApiFactory.getFlightRealizationEntity());
    }


    @Test
    void findById_flightRealizationNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(flightRealizationFacade.getFlightRealization(id)).thenThrow(new FlightRealizationNotFoundException());

        mockMvc.perform(get("/flightrealization/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        Mockito.verify(flightRealizationFacade, Mockito.times(1)).getFlightRealization(id);
    }

    @Test
    void updateflightRealization_flightRealizationFound_returnsIssueReport() throws Exception {
        UUID id = new UUID(0x1, 0xf);
        cz.muni.fi.pa165.api.flight.FlightRealization updatedFlightRealization = TestApiFactory.getFlightRealizationEntity();
        Mockito.when(flightRealizationFacade.updateFlightRealization(Mockito.eq(id), Mockito.any(FlightRealizationRequest.class))).thenReturn(updatedFlightRealization);

        String requestJson = "{" +
                "\"id\":\"" + id + "\"," +
                "\"flightId\":\"" + new UUID(0x2, 0xf) + "\"," +
                "\"report\":\"report\"," +
                "\"duration\":\"" + Duration.ofHours(1) + "\"," +
                "\"departureTime\":\"" + LocalDateTime.of(2002, 2, 2, 2, 2, 2) + "\"," +
                "\"arrivalTime\":\"" + LocalDateTime.of(2002, 2, 2, 2, 2, 2) + "\"," +
                "\"kilometersFlown\":" + 850.0 +
                "}";
        String responseJson = mockMvc.perform(put("/flightrealization/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                )
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        FlightRealization response = ObjectConverter.convertJsonToObject(responseJson, FlightRealization.class);
        assertThat(response).isEqualTo(updatedFlightRealization);
        Mockito.verify(flightRealizationFacade, Mockito.times(1))
                .updateFlightRealization(Mockito.eq(id), Mockito.any(FlightRealizationRequest.class));
    }


    @Test
    public void updateflightRealization_flightRealizationNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();

        String requestJson = "{" +
                "\"id\":\"" + id + "\"," +
                "\"flightId\":\"" + new UUID(0x2, 0xf) + "\"," +
                "\"report\":\"report\"," +
                "\"duration\":\"" + Duration.ofHours(1) + "\"," +
                "\"departureTime\":\"" + LocalDateTime.of(2002, 2, 2, 2, 2, 2) + "\"," +
                "\"arrivalTime\":\"" + LocalDateTime.of(2002, 2, 2, 2, 2, 2) + "\"," +
                "\"kilometersFlown\":" + 850.0 +
                "}";

        Mockito.when(flightRealizationFacade.updateFlightRealization(Mockito.eq(id), Mockito.any(FlightRealizationRequest.class))).thenThrow(new FlightRealizationNotFoundException());

        mockMvc.perform(put("/flightrealization/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isNotFound());

        Mockito.verify(flightRealizationFacade, Mockito.times(1)).updateFlightRealization(Mockito.eq(id), Mockito.any(FlightRealizationRequest.class));
    }


    @Test
    public void deleteflightRealization_flightRealizationFound() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.doNothing().when(flightRealizationFacade).deleteFlightRealization(id);
        RequestBuilder request = MockMvcRequestBuilders.delete("/flightrealization/{id}", id).accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(request).andExpect(status().isOk());
        Mockito.verify(flightRealizationFacade, Mockito.times(1)).deleteFlightRealization(id);
    }
}
