package cz.muni.fi.pa165.api.airport.requests;

import lombok.Data;

@Data
public class AirportRequest {
    private String name;
    private String capacity;
    private double landingPrice;
    private double longitude;
    private double latitude;
    private String city;
    private String country;
}
