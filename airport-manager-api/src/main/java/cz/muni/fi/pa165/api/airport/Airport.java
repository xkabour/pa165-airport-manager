package cz.muni.fi.pa165.api.airport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Airport {

    private UUID id;
    private String name;
    private String capacity;
    private double landingPrice;
    private double longitude;
    private double latitude;
    private String city;
    private String country;
}
