package cz.muni.fi.pa165.api.employee;

import cz.muni.fi.pa165.api.plane.PlaneType;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Data
public class Pilot extends Employee {

    private Set<PlaneType> licences;
}
