package cz.muni.fi.pa165.api.employee;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Data
public class Steward extends Employee{
    private Set<String> languages;
}
