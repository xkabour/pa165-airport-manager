package cz.muni.fi.pa165.api.flight;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class IssueReport {

    private UUID id;
    private UUID planeId;
    private UUID flightEncountered;
    private String description;

}
