package cz.muni.fi.pa165.api.employee.client;

import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.plane.PlaneType;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;
import java.util.Set;

public interface HrClientDescriptor {

    @GET("/employee/pilot/licenced")
    Call<List<Pilot>> listLicencedPilots(@Query("planeType") PlaneType licence);

}
