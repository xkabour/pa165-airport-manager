package cz.muni.fi.pa165.api.flight.requests;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
public class FlightRequest {

    private UUID origin;
    private UUID destination;
    private UUID planeId;
    private Set<UUID> pilotIds;
    private Set<UUID> stewardIds;
}
