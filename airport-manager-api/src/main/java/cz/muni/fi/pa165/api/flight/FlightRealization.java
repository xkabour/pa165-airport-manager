package cz.muni.fi.pa165.api.flight;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class FlightRealization {

    private UUID id;
    private UUID flightId;
    private String report;

    private Duration duration;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;

    private double kilometersFlown;

}
