package cz.muni.fi.pa165.api.employee.requests;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class StewardRequest extends EmployeeRequest {

    private List<String> languages;

    public StewardRequest(String name, String surname, boolean gender, LocalDate dateOfBirth) {
        super(name, surname, gender, dateOfBirth);
    }

    public StewardRequest() {
    }
}