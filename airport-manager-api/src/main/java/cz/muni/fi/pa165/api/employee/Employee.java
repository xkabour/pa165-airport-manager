package cz.muni.fi.pa165.api.employee;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

@Data
public class Employee implements Serializable {

    private UUID id;
    private String name;
    private String surname;
    private Boolean gender;

    private LocalDate dateOfBirth;

    private LocalDate hired;
    private LocalDate terminated;

}
