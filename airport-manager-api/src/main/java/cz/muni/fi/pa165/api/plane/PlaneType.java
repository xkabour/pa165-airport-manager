package cz.muni.fi.pa165.api.plane;

public enum PlaneType {
    CESSNA,
    AIRBUS_A380,
    AIRBUS_A350,
    AIRBUS_A320,
    AIRBUS_A310,
}
