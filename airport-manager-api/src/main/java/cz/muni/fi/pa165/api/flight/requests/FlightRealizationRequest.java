package cz.muni.fi.pa165.api.flight.requests;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@AllArgsConstructor
public class FlightRealizationRequest {

    private UUID flightId;
    private String report;
    private Duration duration;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    private double kilometersFlown;
}
