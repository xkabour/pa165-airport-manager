package cz.muni.fi.pa165.api.plane.requests;

import cz.muni.fi.pa165.api.plane.PlaneType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlaneRequest {

    private String name;
    private PlaneType planeType;
    private Integer maxCapacity;
    private Short pilotsRequired;

}
