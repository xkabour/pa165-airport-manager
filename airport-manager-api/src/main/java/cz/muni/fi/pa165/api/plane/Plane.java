package cz.muni.fi.pa165.api.plane;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Plane {
    private UUID id;
    private String name;
    private PlaneType planeType;
    private int maxCapacity;
    private short pilotsRequired;
}
