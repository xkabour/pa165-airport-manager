package cz.muni.fi.pa165.api.flight;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Flight {

    private UUID id;
    private UUID planeId;
    private Set<UUID> pilotIds;
    private Set<UUID> stewardIds;
    private UUID origin;
    private UUID destination;
}
