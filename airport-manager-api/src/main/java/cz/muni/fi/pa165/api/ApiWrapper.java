package cz.muni.fi.pa165.api;

import lombok.extern.slf4j.Slf4j;
import retrofit2.Call;
import retrofit2.Response;

public class ApiWrapper {

    private static final String DEFAULT_MESSAGE = "Unable to process response body!";


    public <T> T handleCall(Call<T> call) throws Exception {
        long startTime = System.currentTimeMillis();
        int retries = 0;

        try {
            Response<T> response = call.clone().execute();
            return handleResponse(response);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Failed to get a successful response", ex);
        }

    }

    private <T> T handleResponse(Response<T> response) throws Exception {
        if (response.isSuccessful()) {
            System.out.println(response.body());
            return response.body();
        }

        throw new Exception(response.errorBody().string());
    }
}

