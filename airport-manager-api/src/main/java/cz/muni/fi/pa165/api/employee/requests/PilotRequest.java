package cz.muni.fi.pa165.api.employee.requests;

import cz.muni.fi.pa165.api.plane.PlaneType;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
public class PilotRequest extends EmployeeRequest {

    private Set<PlaneType> licences;

    public PilotRequest(String name, String surname, boolean gender, LocalDate dateOfBirth) {
        super(name, surname, gender, dateOfBirth);
    }

    public PilotRequest() {
    }
}
