package cz.muni.fi.pa165.api.employee.requests;

import lombok.*;

import java.time.LocalDate;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmployeeRequest {
    private String name;
    private String surname;
    private Boolean gender;
    private LocalDate dateOfBirth;
}
