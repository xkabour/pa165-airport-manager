package cz.muni.fi.pa165.api.flight.requests;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class IssueReportRequest {

    private UUID planeId;
    private UUID flightEncountered;
    private String description;
}
