package cz.muni.fi.pa165.api.employee.client;

import cz.muni.fi.pa165.api.ApiWrapper;
import cz.muni.fi.pa165.api.employee.Pilot;
import cz.muni.fi.pa165.api.plane.PlaneType;
import lombok.RequiredArgsConstructor;
import okhttp3.MultipartBody;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
public class HrClient extends ApiWrapper {

    private final HrClientDescriptor descriptor;
    public List<Pilot> listLicencedPilots(PlaneType licence) throws Exception {
        return handleCall(descriptor.listLicencedPilots(licence));
    }

}
