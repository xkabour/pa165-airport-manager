#/bin/bash
set -e
COOKIE="JSESSIONID=B392BE31D5FC2ECFB3484E6760A6E363"

echo "Cookie: ${COOKIE}"

echo "We will first register new pilot:"
PILOT=$(curl --silent -X POST "http://localhost:8080/employee/pilot" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"dateOfBirth\": \"2024-05-22\",  \"gender\": true,  \"licences\": [    \"AIRBUS_A310\",    \"CESSNA\"  ],  \"name\": \"Robert\",  \"surname\": \"Letadlo\"}")
PILOT_ID=$(echo ${PILOT} | jq -r '.id')
echo ${PILOT}
echo ""
read

echo "We can also search for available pilots:":
echo "our values are: Pilot: ${PILOT_ID}, Plane: ${PLANE_ID}, Source Airport: ${SRC_AIRPORT_ID}, Dest Airport: ${DEST_AIRPORT_ID}"
curl --silent -X GET "http://localhost:8083/flights/availablePilots?from=2023-05-22&licenceType=AIRBUS_A350&to=2024-05-22" -H  "accept: */*"
echo ""
read

echo "Then, we need to register a plane for our new pilot:"
PLANE=$(curl --silent --header "Cookie: ${COOKIE}"  -X POST "http://localhost:8082/plane" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"maxCapacity\": 5,  \"name\": \"Kamikaze\",  \"pilotsRequired\": 1,  \"planeType\": \"CESSNA\"}")
PLANE_ID=$(echo ${PLANE} | jq -r '.id')
echo ${PLANE}
echo ""
read

echo "Then we need an airport to fly from"
SRC_AIRPORT=$(curl --silent -X POST "http://localhost:8081/airport" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"capacity\": \"1000\",  \"city\": \"Brno\",  \"country\": \"Czechia\",  \"landingPrice\": 100,  \"latitude\": 10,  \"longitude\": 10,  \"name\": \"Letiste Turany\"}")
SRC_AIRPORT_ID=$(echo ${SRC_AIRPORT} | jq -r '.id')
echo ${SRC_AIRPORT}
echo ""
read


echo "Then we need an airport to fly to":
DEST_AIRPORT=$(curl --silent -X POST "http://localhost:8081/airport" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"capacity\": \"10000\",  \"city\": \"Praha\",  \"country\": \"Czechia\",  \"landingPrice\": 10000,  \"latitude\": 25,  \"longitude\": 100,  \"name\": \"Letiste Ruzyne\"}")
DEST_AIRPORT_ID=$(echo ${DEST_AIRPORT} | jq -r '.id')
echo ${DEST_AIRPORT}
echo ""
read


echo "Then we can register a plan for the flight":
echo "our values are: Pilot: ${PILOT_ID}, Plane: ${PLANE_ID}, Source Airport: ${SRC_AIRPORT_ID}, Dest Airport: ${DEST_AIRPORT_ID}"
FLIGHT=$(curl --silent -X POST "http://localhost:8083/flights" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"destination\": \"${DEST_AIRPORT_ID}\",  \"origin\": \"${SRC_AIRPORT_ID}\",  \"pilotIds\": [    \"${PILOT_ID}\"  ],  \"planeId\": \"${PLANE_ID}\",  \"stewardIds\": []}")
FLIGHT_ID=$(echo ${FLIGHT} | jq -r '.id')
echo ${FLIGHT}
echo ""
read


echo "Flight id is ${FLIGHT_ID}"
read

#echo "Lastly we record the details after the flight":
#curl -X POST "http://localhost:8083/flightrealization" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"arrivalTime\": \"2024-05-22T11:04:32.609Z\",  \"departureTime\": \"2024-05-22T11:04:32.609Z\",  \"duration\" : {\"seconds\": 300 },  \"flightId\": \"${FLIGHT_ID}\",  \"kilometersFlown\": 250,  \"report\": \"All good, minor technical difficulties\"}"
#echo ""
#read

echo "If we encountered some difficulties, we can also report them:"
curl --silent -X POST "http://localhost:8083/issuereports" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"description\": \"Weird sound while extending flaps\",  \"flightEncountered\": \"${FLIGHT_ID}\",  \"planeId\": \"${PLANE_ID}\"}"
echo ""
read

echo "Lastly we can check the details about the flight we just made:"
curl --silent -X GET "http://localhost:8083/flights/${FLIGHT_ID}" -H  "accept: */*"

echo ""
echo "========================================="

echo "What happens if we try invalid flight:":
echo "our values are: Pilot: ${PILOT_ID}, Plane: ${PLANE_ID}, Source Airport: ${SRC_AIRPORT_ID}, Dest Airport: ${DEST_AIRPORT_ID}"
FLIGHT=$(curl --silent -X POST "http://localhost:8083/flights" -H  "accept: */*" -H  "Content-Type: application/json" -d "{  \"destination\": \"${DEST_AIRPORT_ID}\",  \"origin\": \"${SRC_AIRPORT_ID}\",  \"pilotIds\": [    \"${PILOT_ID}\"  ],  \"planeId\": null,  \"stewardIds\": []}")
FLIGHT_ID=$(echo ${FLIGHT} | jq -r '.id')
echo ${FLIGHT}
echo ""
read
