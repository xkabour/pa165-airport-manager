#!/bin/bash

cat ./db-management/clean/clean-hr-service.sql | docker-compose exec -T hr-service-db psql -U postgres -d hr -p 5432
cat ./db-management/clean/clean-airports-service.sql | docker-compose exec -T airports-service-db psql -U postgres -d airports -p 5432
cat ./db-management/clean/clean-flights-service.sql | docker-compose exec -T flights-service-db psql -U postgres -d flights -p 5432
cat ./db-management/clean/clean-planes-service.sql | docker-compose exec -T planes-service-db psql -U postgres -d planes   -p 5432