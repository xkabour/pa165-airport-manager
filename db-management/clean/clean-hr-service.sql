SET search_path TO hr;

DELETE FROM language;
DELETE FROM pilot_licence;
DELETE FROM stewards;
DELETE FROM pilots;
DELETE FROM employee;