-- Inserting into flight table
INSERT INTO flight (id, origin, destination, plane_id) VALUES
('f0419160-19c7-4eab-bf28-c5ef98f69735', 'a88c9865-3a35-4a89-a5f0-d02b8fbef0bb', '35a1a2c3-68b8-4b45-a43a-54d08fa611d3', '1b0d6f25-1a5e-4b98-ae1b-0249f4b4d1d2'),
('f6e9c3e5-49e2-4fbf-a3f7-3c27a4e17309', '9248cf6e-fd96-4dd6-af68-4d0c6ed24d2d', 'a1df2c89-0d4a-4e47-80e7-c7f542f0d62b', '5c2aaf75-17d4-4e90-a9e4-62027f48f4b3'),
('1a52a2f0-22da-4371-8f14-08d6a4eb9b1b', 'a1df2c89-0d4a-4e47-80e7-c7f542f0d62b', '35a1a2c3-68b8-4b45-a43a-54d08fa611d3', '7e7bde34-2f7e-4d2d-9178-07d1d42e1f85'),
('5f3b8a57-0c09-4513-9242-3270d13c5c8a', '35a1a2c3-68b8-4b45-a43a-54d08fa611d3', 'b9d3f4a5-6e5d-4e54-ae79-7dc78e2a9bfb', '9d5f4a2b-4f6d-4c78-8b92-8315a6c2e8d2'),
('b72ef63c-9b7b-4ef8-9d99-25ae84806a76', 'a88c9865-3a35-4a89-a5f0-d02b8fbef0bb', 'b9d3f4a5-6e5d-4e54-ae79-7dc78e2a9bfb', 'e2a3d1e4-60a5-456c-8b8f-5295f6d4d8e9')
ON CONFLICT (id) DO NOTHING;

-- Inserting into flightrealization table
INSERT INTO flightrealization (id, flight_id, report, duration, departure_time, arrival_time, kilometers_flown) VALUES
('e78f82e1-0a59-4b8c-8cfb-516946a0a158', 'f0419160-19c7-4eab-bf28-c5ef98f69735', 'Smooth flight', 10, '2024-05-20T08:00:00', '2024-05-20T11:30:00', 2800.00),
('ec6f2f3d-42b0-4d82-8f37-2a5f9739b9e1', 'f6e9c3e5-49e2-4fbf-a3f7-3c27a4e17309', 'Delayed due to weather conditions', 12, '2024-05-20T12:30:00', '2024-05-21T00:15:00', 9500.00),
('4ef85bd2-f1e4-4743-82cd-13421d4d05e9', '1a52a2f0-22da-4371-8f14-08d6a4eb9b1b', 'On-time departure and arrival', 3, '2024-05-20T15:45:00', '2024-05-20T21:00:00', 4200.00),
('6a31aefa-f964-44bb-82de-f1f4b8631d59', '5f3b8a57-0c09-4513-9242-3270d13c5c8a', 'Minor turbulence experienced during flight', 4, '2024-05-20T06:00:00', '2024-05-20T20:30:00', 11200.00),
('b365b4fc-41b5-4d5f-af37-d5a8e1dd3394', 'b72ef63c-9b7b-4ef8-9d99-25ae84806a76', 'Flight landed ahead of schedule', 4, '2024-05-20T10:00:00', '2024-05-20T14:00:00', 3200.00)
ON CONFLICT (id) DO NOTHING;

-- Inserting into issue table
INSERT INTO issue (id, flight_encountered, description, plane_id) VALUES
('eda9a571-78cc-4dd8-b5cb-728b75d7869f', 'f0419160-19c7-4eab-bf28-c5ef98f69735', 'Engine malfunction', '1b0d6f25-1a5e-4b98-ae1b-0249f4b4d1d2')
ON CONFLICT (id) DO NOTHING;

INSERT INTO issue (id, flight_encountered, description, plane_id) VALUES
('eda9a571-78cc-4dd8-b5cb-728b75d7869f', 'f0419160-19c7-4eab-bf28-c5ef98f69735', 'Engine malfunction', '1b0d6f25-1a5e-4b98-ae1b-0249f4b4d1d2')
ON CONFLICT (id) DO NOTHING;

INSERT INTO flight_pilot_ids(flight_id, steward_id) VALUES
('f0419160-19c7-4eab-bf28-c5ef98f69735', '1d2a647c-8c3a-4e6f-814a-6a7f1c58c97e'),
('f0419160-19c7-4eab-bf28-c5ef98f69735', '5e8b8d1a-531d-45e3-8efc-7e2f437b5f28'),
('f6e9c3e5-49e2-4fbf-a3f7-3c27a4e17309', '5e8b8d1a-531d-45e3-8efc-7e2f437b5f28'),
('f6e9c3e5-49e2-4fbf-a3f7-3c27a4e17309', '1d2a647c-8c3a-4e6f-814a-6a7f1c58c97e');

INSERT INTO flight_steward_ids(flight_id, steward_id) VALUES
('f0419160-19c7-4eab-bf28-c5ef98f69735', '73f9c9d5-718f-4fae-9910-49cfcd1b4503'),
('f0419160-19c7-4eab-bf28-c5ef98f69735', '4a7e9390-8e54-4b15-9db3-3a945b1e0216'),
('f6e9c3e5-49e2-4fbf-a3f7-3c27a4e17309', '73f9c9d5-718f-4fae-9910-49cfcd1b4503'),
('f6e9c3e5-49e2-4fbf-a3f7-3c27a4e17309', '4a7e9390-8e54-4b15-9db3-3a945b1e0216');

