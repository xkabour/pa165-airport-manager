SET search_path TO airport;


INSERT INTO airport (id, name, capacity, landing_price, longitude, latitude, city, country)
VALUES ('a88c9865-3a35-4a89-a5f0-d02b8fbef0bb', 'John F. Kennedy International Airport', 'Large', 50000.00, -73.7781, 40.6413, 'New York City', 'United States') ON CONFLICT (id) DO NOTHING;

INSERT INTO airport (id, name, capacity, landing_price, longitude, latitude, city, country)
VALUES ('9248cf6e-fd96-4dd6-af68-4d0c6ed24d2d', 'Heathrow Airport', 'Large', 55000.00, -0.4610, 51.4700, 'London', 'United Kingdom') ON CONFLICT (id) DO NOTHING;

INSERT INTO airport (id, name, capacity, landing_price, longitude, latitude, city, country)
VALUES ('a1df2c89-0d4a-4e47-80e7-c7f542f0d62b', 'Los Angeles International Airport', 'Large', 48000.00, -118.4079, 33.9416, 'Los Angeles', 'United States') ON CONFLICT (id) DO NOTHING;

INSERT INTO airport (id, name, capacity, landing_price, longitude, latitude, city, country)
VALUES ('35a1a2c3-68b8-4b45-a43a-54d08fa611d3', 'Beijing Capital International Airport', 'Large', 60000.00, 116.5871, 40.0799, 'Beijing', 'China') ON CONFLICT (id) DO NOTHING;

INSERT INTO airport (id, name, capacity, landing_price, longitude, latitude, city, country)
VALUES ('b9d3f4a5-6e5d-4e54-ae79-7dc78e2a9bfb', 'Dubai International Airport', 'Large', 70000.00, 55.3644, 25.2532, 'Dubai', 'United Arab Emirates') ON CONFLICT (id) DO NOTHING;
