SET search_path TO planes;

INSERT INTO plane (id, name, plane_type, max_capacity, pilots_required)
VALUES ('1b0d6f25-1a5e-4b98-ae1b-0249f4b4d1d2', 0, 0, 4, 1) ON CONFLICT (id) DO NOTHING;

INSERT INTO plane (id, name, plane_type, max_capacity, pilots_required)
VALUES ('5c2aaf75-17d4-4e90-a9e4-62027f48f4b3', 1, 1, 853, 4) ON CONFLICT (id) DO NOTHING;

INSERT INTO plane (id, name, plane_type, max_capacity, pilots_required)
VALUES ('7e7bde34-2f7e-4d2d-9178-07d1d42e1f85', 2, 2, 440, 2) ON CONFLICT (id) DO NOTHING;

INSERT INTO plane (id, name, plane_type, max_capacity, pilots_required)
VALUES ('9d5f4a2b-4f6d-4c78-8b92-8315a6c2e8d2', 3, 3, 240, 2) ON CONFLICT (id) DO NOTHING;

INSERT INTO plane (id, name, plane_type, max_capacity, pilots_required)
VALUES ('e2a3d1e4-60a5-456c-8b8f-5295f6d4d8e9', 4, 4, 280, 2) ON CONFLICT (id) DO NOTHING;