SET search_path TO hr;

-- Insert into language table
INSERT INTO language (id, language) VALUES
('73f9c9d5-718f-4fae-9910-49cfcd1b4503', 'English'),
('73f9c9d5-718f-4fae-9910-49cfcd1b4503', 'Spanish'),
('4a7e9390-8e54-4b15-9db3-3a945b1e0216', 'French'),
('4a7e9390-8e54-4b15-9db3-3a945b1e0216', 'German'),
('4a7e9390-8e54-4b15-9db3-3a945b1e0216', 'Chinese')
ON CONFLICT (id) DO NOTHING;

-- Insert into pilot_licence table
INSERT INTO pilot_licence (id, type) VALUES
('1d2a647c-8c3a-4e6f-814a-6a7f1c58c97e', 0), -- CESSNA
('1d2a647c-8c3a-4e6f-814a-6a7f1c58c97e', 1), -- AIRBUS_A380
('5e8b8d1a-531d-45e3-8efc-7e2f437b5f28', 2), -- AIRBUS_A350
('5e8b8d1a-531d-45e3-8efc-7e2f437b5f28', 3), -- AIRBUS_A320
('5e8b8d1a-531d-45e3-8efc-7e2f437b5f28', 4) -- AIRBUS_A310
ON CONFLICT (id) DO NOTHING;

-- Insert into employee table
INSERT INTO employee (id, name, surname, gender, date_of_birth, hired) VALUES
('1d2a647c-8c3a-4e6f-814a-6a7f1c58c97e', 'James', 'Smith', TRUE, '1980-05-22', '2010-04-12'),
('5e8b8d1a-531d-45e3-8efc-7e2f437b5f28', 'Linda', 'Taylor', FALSE, '1985-08-14', '2012-11-09'),
('73f9c9d5-718f-4fae-9910-49cfcd1b4503', 'Samantha', 'Brown', FALSE, '1990-02-10', '2015-06-18'),
('4a7e9390-8e54-4b15-9db3-3a945b1e0216', 'Michael', 'Wilson', TRUE, '1987-11-30', '2013-09-27')
ON CONFLICT (id) DO NOTHING;

-- Insert into pilots table
INSERT INTO pilots (employee_id) VALUES
('1d2a647c-8c3a-4e6f-814a-6a7f1c58c97e'),
('5e8b8d1a-531d-45e3-8efc-7e2f437b5f28')
ON CONFLICT (employee_id) DO NOTHING;

-- Insert into stewards table
INSERT INTO stewards (employee_id) VALUES
('73f9c9d5-718f-4fae-9910-49cfcd1b4503'),
('4a7e9390-8e54-4b15-9db3-3a945b1e0216')
ON CONFLICT (employee_id) DO NOTHING;