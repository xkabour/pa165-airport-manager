package cz.muni.fi.pa165.airport.dao;

import cz.muni.fi.pa165.airport.Constants;
import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "airport", schema = Constants.AIRPORT_SCHEMA)
public class Airport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String name;
    private String capacity;
    private Double landingPrice;
    private Double longitude;
    private Double latitude;
    private String city;
    private String country;
}
