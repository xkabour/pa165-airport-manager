package cz.muni.fi.pa165.airport.rest;

import cz.muni.fi.pa165.airport.facade.AirportFacade;
import cz.muni.fi.pa165.api.airport.Airport;
import cz.muni.fi.pa165.api.airport.requests.AirportRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AirportController {

    private final AirportFacade airportFacade;

    @PostMapping("/airport")
    public Airport create(@RequestBody AirportRequest request) {
        return airportFacade.create(request);
    }

    @GetMapping("/airport/{id}")
    public Airport get(@PathVariable UUID id) {
        return airportFacade.get(id);
    }

    @PutMapping("/airport/{id}")
    public Airport update(@PathVariable UUID id, @RequestBody AirportRequest request) {
        return airportFacade.update(id, request);
    }

    @GetMapping("/airport/country")
    public List<Airport> getByCountry(@RequestParam String country) {
        return airportFacade.getByCountry(country);
    }
}
