package cz.muni.fi.pa165.airport.facade;

import cz.muni.fi.pa165.airport.service.AirportService;
import cz.muni.fi.pa165.api.airport.Airport;
import cz.muni.fi.pa165.api.airport.requests.AirportRequest;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AirportFacade {

    private final AirportService airportService;

    private final ModelMapper modelMapper = new ModelMapper();

    public Airport create(AirportRequest airportRequest) {
        var result = airportService.create(modelMapper.map(airportRequest, cz.muni.fi.pa165.airport.dao.Airport.class));
        return modelMapper.map(result, Airport.class);
    }

    public Airport get(UUID id) {
        var result = airportService.get(id);
        return modelMapper.map(result, Airport.class);
    }

    public Airport update(UUID id, AirportRequest airportRequest) {
        var result = airportService.update(id, modelMapper.map(airportRequest, cz.muni.fi.pa165.airport.dao.Airport.class));
        return modelMapper.map(result, Airport.class);
    }

    public List<Airport> getByCountry(String country) {
        var result = airportService.getByCountry(country);
        return result.stream().map(p -> modelMapper.map(p, Airport.class)).toList();
    }
}
