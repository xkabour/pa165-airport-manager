package cz.muni.fi.pa165.airport.observations.health;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomHealthIndicator implements HealthIndicator {

    @Autowired
    private HealthProvider healthProvider;

    @Override
    public Health health() {
        boolean systemHealth = healthProvider.getSystemHealth();
        if (systemHealth) {
            return Health.up().build();
        } else {
            return Health.down().withDetail("error", "injected failure").build();
        }
    }
}
