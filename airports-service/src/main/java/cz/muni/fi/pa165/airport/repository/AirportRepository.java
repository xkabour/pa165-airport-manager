package cz.muni.fi.pa165.airport.repository;

import cz.muni.fi.pa165.airport.dao.Airport;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface AirportRepository extends CrudRepository<Airport, UUID> {
    List<Airport> findByCountry(String country);
}
