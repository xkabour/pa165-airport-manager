package cz.muni.fi.pa165.airport.service;

import cz.muni.fi.pa165.airport.dao.Airport;
import cz.muni.fi.pa165.airport.exception.AirportNotFoundException;
import cz.muni.fi.pa165.airport.repository.AirportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AirportService {

    private final AirportRepository airportRepository;

    public Airport create(Airport airport) {
        return airportRepository.save(airport);
    }

    public Airport get(UUID id) {
        return airportRepository.findById(id).orElseThrow(AirportNotFoundException::new);
    }

    public Airport update(UUID id, Airport airportUpdate) {
        var airport = airportRepository.findById(id).orElseThrow(AirportNotFoundException::new);
        if (airportUpdate.getCapacity() != null) {
            airport.setCapacity(airportUpdate.getCapacity());
        }
        if (airportUpdate.getLandingPrice() != null) {
            airport.setLandingPrice(airportUpdate.getLandingPrice());
        }
        if (airportUpdate.getName() != null) {
            airport.setName(airportUpdate.getName());
        }
        return airportRepository.save(airport);
    }

    public List<Airport> getByCountry(String country) {
        return airportRepository.findByCountry(country);
    }
}
