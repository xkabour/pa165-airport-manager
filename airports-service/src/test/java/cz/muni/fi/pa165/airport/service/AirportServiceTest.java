package cz.muni.fi.pa165.airport.service;

import cz.muni.fi.pa165.airport.dao.Airport;
import cz.muni.fi.pa165.airport.exception.AirportNotFoundException;
import cz.muni.fi.pa165.airport.repository.AirportRepository;
import cz.muni.fi.pa165.airport.utils.TestDaoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AirportServiceTest {

    @Mock
    private AirportRepository airportRepository;

    @InjectMocks
    private AirportService airportService;

    @Test
    void createAirport() {
        Airport airport = TestDaoFactory.getAirportEntity();
        Mockito.when(airportRepository.save(airport)).thenReturn(airport);
        assertEquals(airport, airportService.create(airport));
        Mockito.verify(airportRepository, Mockito.times(1)).save(airport);
    }

    @Test
    void getById_airportFound_returnsAirport() {
        UUID id = new UUID(0x1, 0xf);
        Airport airport = TestDaoFactory.getAirportEntity();
        // Arrange
        Mockito.when(airportRepository.findById(id)).thenReturn(Optional.of(airport));

        // Act
        Airport foundEntity = airportService.get(id);

        // Assert
        assertThat(foundEntity).isEqualTo(airport);
    }

    @Test
    void getById_airportNotFound_throwsResourceNotFoundException() {
        UUID id = UUID.randomUUID();

        assertThrows(AirportNotFoundException.class, () -> Optional
                .ofNullable(airportService.get(id))
                .orElseThrow(AirportNotFoundException::new));
    }

    @Test
    void updateAirport_airportFound() {
        UUID id = new UUID(0x1, 0xf);
        Airport existingAirport = TestDaoFactory.getAirportEntity();

        Airport update = new Airport();
        update.setName("newName");

        Airport updatedAirport = TestDaoFactory.getAirportEntity();
        updatedAirport.setName("newName");

        Mockito.when(airportRepository.findById(id)).thenReturn(Optional.of(existingAirport));
        Mockito.when(airportRepository.save(Mockito.any(Airport.class))).thenAnswer(invocation -> invocation.getArgument(0));

        assertEquals(updatedAirport, airportService.update(id, update));
        Mockito.verify(airportRepository, Mockito.times(1)).save(updatedAirport);
    }

    @Test
    void updateAirport_airportNotFound_throwsAirportNotFoundException() {
        UUID id = UUID.randomUUID();
        Airport airport = new Airport();

        assertThrows(AirportNotFoundException.class, () -> Optional
                .ofNullable(airportService.update(id, airport))
                .orElseThrow(AirportNotFoundException::new));

        Mockito.verify(airportRepository, Mockito.times(0)).save(Mockito.any());
    }

    @Test
    void getByCountry() {
        Airport airport1 = TestDaoFactory.getAirportEntity();
        Airport airport2 = TestDaoFactory.getAirportEntity();
        airport2.setId(UUID.randomUUID());
        List<Airport> airportsSet = List.of(airport1, airport2);
        // Arrange
        Mockito.when(airportRepository.findByCountry("Country")).thenReturn(airportsSet);

        // Act
        List<Airport> foundEntity = airportService.getByCountry("Country");

        // Assert
        assertThat(foundEntity).isEqualTo(airportsSet);
    }
}