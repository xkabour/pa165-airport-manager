package cz.muni.fi.pa165.airport.utils;

import cz.muni.fi.pa165.api.airport.Airport;
import cz.muni.fi.pa165.api.airport.requests.AirportRequest;
import cz.muni.fi.pa165.api.plane.PlaneType;
import cz.muni.fi.pa165.api.plane.requests.PlaneRequest;

import java.util.UUID;

public class TestApiFactory {
    public static Airport getAirportEntity() {
        Airport airport = new Airport();
        airport.setName("Name");
        airport.setCapacity("15000");
        airport.setLandingPrice(500.0);
        airport.setLongitude(50.123456);
        airport.setLatitude(14.987654);
        airport.setCity("Example City");
        airport.setCountry("Example Country");

        airport.setId(new UUID(0x1, 0xf));

        return airport;
    }

    public static AirportRequest getAirportRequest() {
        var airport =  new AirportRequest();
        airport.setName("Name");
        airport.setCapacity("15000");
        airport.setLandingPrice(500.0);
        airport.setLongitude(50.123456);
        airport.setLatitude(14.987654);
        airport.setCity("Example City");
        airport.setCountry("Example Country");
        return airport;
    }

}
