package cz.muni.fi.pa165.airport.rest;

import cz.muni.fi.pa165.airport.exception.AirportNotFoundException;
import cz.muni.fi.pa165.airport.facade.AirportFacade;
import cz.muni.fi.pa165.airport.utils.ObjectConverter;
import cz.muni.fi.pa165.airport.utils.TestApiFactory;
import cz.muni.fi.pa165.api.airport.Airport;
import cz.muni.fi.pa165.api.airport.requests.AirportRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {AirportController.class})
class AirportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AirportFacade airportFacade;

    @Test
    void findById_airportFound_returnsAirport() throws Exception {
        UUID id = UUID.fromString("00000000-0000-0001-0000-00000000000f");
        Mockito.when(airportFacade.get(id)).thenReturn(TestApiFactory.getAirportEntity());

        String responseJson = mockMvc.perform(get("/airport/{id}", id)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Airport response = ObjectConverter.convertJsonToObject(responseJson, Airport.class);

        assertThat(responseJson).isEqualTo("{\"id\":\"00000000-0000-0001-0000-00000000000f\",\"name\":\"Name\",\"capacity\":\"15000\",\"landingPrice\":500.0,\"longitude\":50.123456,\"latitude\":14.987654,\"city\":\"Example City\",\"country\":\"Example Country\"}");
        assertThat(response).isEqualTo(TestApiFactory.getAirportEntity());
    }

    @Test
    void findById_airportNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(airportFacade.get(id)).thenThrow(new AirportNotFoundException());

        mockMvc.perform(get("/airport/{id}", id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        Mockito.verify(airportFacade, Mockito.times(1)).get(id);
    }


    @Test
    void updateAirport_airportFound_returnsAirport() throws Exception {
        UUID id = UUID.fromString("00000000-0000-0001-0000-00000000000f");
        Airport updatedAirport = TestApiFactory.getAirportEntity();
        Mockito.when(airportFacade.update(Mockito.eq(id), Mockito.any(AirportRequest.class))).thenReturn(updatedAirport);

        String requestJson = "{\"name\":\"UpdatedName\",\"capacity\":\"Large\",\"landingPrice\":1500.0,\"longitude\":-73.935242,\"latitude\":40.73061,\"city\":\"New York\",\"country\":\"USA\"}";
        String responseJson = mockMvc.perform(put("/airport/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Airport response = ObjectConverter.convertJsonToObject(responseJson, Airport.class);
        assertThat(response).isEqualTo(updatedAirport);
        Mockito.verify(airportFacade, Mockito.times(1)).update(Mockito.eq(id), Mockito.any(AirportRequest.class));
    }

    @Test
    void updateAirport_airportNotFound_throws404() throws Exception {
        UUID id = UUID.randomUUID();
        Mockito.when(airportFacade.update(Mockito.eq(id), Mockito.any(AirportRequest.class))).thenThrow(new AirportNotFoundException());

        mockMvc.perform(put("/airport/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"UpdatedName\",\"capacity\":\"Large\",\"landingPrice\":1500.0,\"longitude\":-73.935242,\"latitude\":40.73061,\"city\":\"New York\",\"country\":\"USA\"}"))
                .andExpect(status().isNotFound());

        Mockito.verify(airportFacade, Mockito.times(1)).update(Mockito.eq(id), Mockito.any(AirportRequest.class));
    }

    @Test
    void getByCountry_returnsAirports() throws Exception {
        String country = "USA";
        List<Airport> airports = new ArrayList<>();
        Airport airport1 = TestApiFactory.getAirportEntity();
        Airport airport2 = TestApiFactory.getAirportEntity();
        airport2.setCountry(country);
        airports.add(airport1);
        airports.add(airport2);

        Mockito.when(airportFacade.getByCountry(country)).thenReturn(airports);

        mockMvc.perform(get("/airport/country").param("country", country).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}