package cz.muni.fi.pa165.airport.utils;

import cz.muni.fi.pa165.airport.dao.Airport;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TestDaoFactory {

    public static Airport getAirportEntity() {
        Airport airport = new Airport();
        airport.setName("Name");
        airport.setCapacity("15000");
        airport.setLandingPrice(500.0);
        airport.setLongitude(50.123456);
        airport.setLatitude(14.987654);
        airport.setCity("Example City");
        airport.setCountry("Example Country");

        airport.setId(new UUID(0x1, 0xf));

        return airport;
    }

}
