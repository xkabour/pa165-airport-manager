package cz.muni.fi.pa165.airport.facade;

import cz.muni.fi.pa165.airport.exception.AirportNotFoundException;
import cz.muni.fi.pa165.airport.service.AirportService;
import cz.muni.fi.pa165.airport.utils.TestApiFactory;
import cz.muni.fi.pa165.airport.utils.TestDaoFactory;
import cz.muni.fi.pa165.api.airport.requests.AirportRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AirportFacadeTest {

    @Mock
    private AirportService airportService;

    @InjectMocks
    private AirportFacade airportFacade;

    @Test
    void createAirport() {
        var apiAirport = TestApiFactory.getAirportEntity();
        apiAirport.setId(null);
        var daoAirport = TestDaoFactory.getAirportEntity();
        daoAirport.setId(null);
        AirportRequest request = TestApiFactory.getAirportRequest();

        when(airportService.create(daoAirport)).thenReturn(daoAirport);
        assertEquals(apiAirport, airportFacade.create(request));
        verify(airportService, times(1)).create(daoAirport);
    }


    @Test
    void getById_airportFound_returnsAirport() {
        UUID id = UUID.randomUUID();
        var daoAirport = TestDaoFactory.getAirportEntity();
        when(airportService.get(id)).thenReturn(daoAirport);

        var result = airportFacade.get(id);
        assertNotNull(result);
        verify(airportService, times(1)).get(id);
    }

    @Test
    void getById_airportNotFound_throwsAirportNotFoundException() {
        UUID id = UUID.randomUUID();
        when(airportService.get(id)).thenThrow(new AirportNotFoundException());
        assertThrows(AirportNotFoundException.class, () -> airportFacade.get(id));
    }

    @Test
    void updateAirport_AirportFound() {
        UUID id = UUID.randomUUID();
        var apiAirport = TestApiFactory.getAirportEntity();
        apiAirport.setId(null);
        var daoAirport = TestDaoFactory.getAirportEntity();
        daoAirport.setId(null);
        var request = TestApiFactory.getAirportRequest();
        Mockito.when(airportService.update(id, daoAirport)).thenReturn(daoAirport);
        assertEquals(apiAirport, airportFacade.update(id, request));
        verify(airportService, times(1)).update(id, daoAirport);
    }

    @Test
    void updateAirport_AirportNotFound() {
        UUID id = UUID.randomUUID();
        when(airportService.update(Mockito.any(), Mockito.any())).thenThrow(new AirportNotFoundException());
        assertThrows(AirportNotFoundException.class, () -> airportFacade.update(id, TestApiFactory.getAirportRequest()));
    }

    @Test
    void getAirportsByType(){
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        var apiAirport1 = TestApiFactory.getAirportEntity();
        apiAirport1.setId(id1);
        var apiAirport2 = TestApiFactory.getAirportEntity();
        apiAirport2.setId(id2);

        var daoAirport1 = TestDaoFactory.getAirportEntity();
        daoAirport1.setId(id1);
        var daoAirport2 = TestDaoFactory.getAirportEntity();
        daoAirport2.setId(id2);
        when(airportService.getByCountry(daoAirport1.getCountry())).thenReturn(List.of(daoAirport1, daoAirport2));
        var result = airportFacade.getByCountry(apiAirport1.getCountry());
        assertEquals(List.of(apiAirport1, apiAirport2), result);
    }
}
